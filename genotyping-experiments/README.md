# README #

The pipeline in this repository was used to generate and analyze the PanGenie results for the HPRC project based on the minigraph-cactus VCF. All analyses are based on the GRCh38 reference genome.

## What the pipeline does ##

This pipeline can be used to re-produce the PanGenie experiments for the HPRC project. The pipeline performs four main steps. 

* **preprocessing**: preprocesses the cactus/minigraph VCF. Filtering steps include removing non-top level bubbles and replacing large top-level bubbles by their nested variants using vcfbub (https://github.com/pangenome/vcfbub), removing sites where less than 80%
of haplotypes have a missing allele and removing alleles not covered by any of the panel haplotypes. Furthermore, allele sequences are decomposed into their nested variants based on their traversals in the graph. This results in
a version of the VCF that has additional INFO tags which annotate each allele by their sequence of nested alleles (referred to "multiallelic VCF" in the following). In addition, a second, biallelic VCF is generated (referred to "biallelic VCF" in the following), which specifies REF and ALT allele of each individual nested
variant. Both VCFs represent the same variant information in different ways, but the biallelic representation simplifies many analyses and comparison to other callsets. Finally, the biallelic VCF is used in order
to determine a list of untypable alleles for each panel sample, which contains all alleles unique to the respective sample, that is, alleles seen only in the sample but in none of the other panel samples.

* **leave-one-out experiment**: PanGenie's genotyping performance is evaluated by repeatedly removing one of the panel samples from the multiallelic VCF, and then genotyping it
using a panel containing the remaining n-1 samples only. PanGenie predicts a genotype for each input variant. The resulting VCF is converted into the biallelic VCF representation, based on the annotations that were added by the
decomposition step. This results in a genotyped version of the variants in the biallelic VCF. The genotypes are next compared to the ground truth genotypes of the left out sample using the weighted genotype concordance
as a metric.

* **population-typing**: PanGenie is run on all 3,202 samples that are part of the 1000 Genomes project. The multiallelic VCF is used as input for PanGenie, producing genotype predictions for all variants it contains.
Genotyped VCFs are again converted to the biallelic VCF representation, based on the annotations in the multiallelic VCF, producing genotypes for all alleles contained in the biallelic VCF.
Results are then evaluated based on trio information and different statistics, and a positive set (=strict) as well as a filtered set (=lenient) are computed containing subsets of variants genotyped with high quality.
In addition, the PanGenie genotypes for SVs are compared to the HGSVC freeze4 SV genotypes, as well as to the 1kGP SV calls.

* **callset-comparison**: The PanGenie genotypes are evaluated based on assembly-based, dipcall truth sets. vcfeval is used to compute precision and recall. Additionally, ground truth variants untypable by PanGenie are determined
based on comparing panel variants of each sample to the ground truth using vcfeval, and then intersecting false negatives for all panel samples. This results in the set of variants untypable by re-typing methods (like PanGenie),
because they were absent in the graph. Precision and recall are additionally computed after excluding these untypable variants from the ground truth.

## How to run the pipeline ##

Paths to input files needed must be specified in the config file: `` config/config.yaml ``
The whole pipeline can then be run using the following command:

``  snakemake --use-conda -j <number of cores>  `` 

Alternatively, different parts of the pipeline can be run individually:

* ``  snakemake preprocessing --use-conda -j <number of cores>  ``  runs only the first part part of the pipeline, which preprocesses the input panel VCF generated from the cactus/minigraph graph, filters the VCF and performs a decomposition of the bubbles represented in this VCF. The files produced here are required for all other parts of the pipeline.
* ``  snakemake leave_one_out --use-conda -j <number of cores>  ``  runs the PanGenie leave-one-out experiment.
* ``  snakemake population_typing --use-conda -j <number of cores>  `` runs PanGenie on all 3,202 genomes of the 1000 Genomes Project and analyzes the results. This also includes defining a positive (=strict) and filtered (=lenient) set of genotypes.
* ``  snakemake callset_comparison --use-conda -j <number of cores>  `` uses assembly-based ground truths in order to evaluate PanGenie's genotypes