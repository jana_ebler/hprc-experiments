import sys
from collections import defaultdict

sample_file = sys.argv[1]
trio_file = sys.argv[2]

samples = []
sample_present = defaultdict(lambda: False)

for s in open(sample_file, 'r'):
	samples.append(s.strip())

for t in open(trio_file, 'r'):
	fields = t.split()
	if not fields[1] in samples:
		continue
	if fields[2] != '0' and not fields[2] in samples:
		continue

	if fields[3] != '0' and not fields[3] in samples:
		continue

	sample_present[fields[1]] = True
	print(t.strip())

for sample in samples:
	if not sample_present[sample]:
		print(sample)
