import sys

output_regions = ["VNTR", "STR", "Other-LCR", "L1", "Alu", "SVA", "ERV", "Other-TE", "Satellite", "Low-repeat", "Other-repeat", "SegDup"]
output_beds = {}

for region in output_regions:
	output_beds[region] = open('GRCh38_' + region + '.bed', 'w')

for line in sys.stdin:
	fields = line.strip().split()
	if int(fields[5]):
		continue
	if fields[11] == "gap":
		continue
	if int(fields[4]) == 1:
		continue
	region = fields[11]
	repeat_type = None
	if region == "mini":
		repeat_type = "VNTR"
	elif region == "micro":
		repeat_type = "STR"
	elif (region == "micro") or (region == "lcr"):
		repeat_type = "Other-LCR"
	elif region == "LINE/L1":
		repeat_type = "L1"
	elif region == "SINE/Alu":
		repeat_type = "Alu"
	elif region == "Retroposon/SVA":
		repeat_type = "SVA"
	elif region == "LTR/ERV":
		repeat_type = "ERV"
	elif (region == "inter") or any([region.startswith(i) for i in ["DNA", "LINE", "SINE", "LTR"]]):
		repeat_type = "Other-TE"
	elif (region.startswith("Satellite") or region in ["alpha", "hsat2/3", "_sat"]):
		repeat_type = "Satellite"
	elif (region == "self") or (region == "none"):
		repeat_type = "Low-repeat"
	elif region == "mixed":
		repeat_type = "Other-repeat"
	elif region == "segdup":
		repeat_type = "SegDup"
	elif region == "partial":
		repeat_type = "Low-repeat"
	else:
		repeat_type = "Other-repeat"

	assert repeat_type in output_beds
	# coordinates seem to be 1-based (instead of 0-based according to BED format), therefore shift first coordinate
	start = str(int(fields[1]) - 1)
	output_beds[repeat_type].write('\t'.join([fields[0], start, fields[2], fields[11]]) + '\n')
	
for region in output_regions:
	output_beds[region].close()
