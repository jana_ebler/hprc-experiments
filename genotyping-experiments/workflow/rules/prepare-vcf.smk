configfile: "config/config.yaml"

assembly_samples = config['assembly_samples']
min_an = int(0.8 * len(assembly_samples) * 2)
nr_males = int(config['nr_males'])
min_an_male = int(0.8 * ( ((len(assembly_samples)-nr_males)*2) + nr_males) )
min_an_female = int(0.8 * nr_males * 2)
print('min_an', min_an)
print('min_an_male', min_an_male)
print('min_an_female', min_an_female)
chromosomes = ['chr' + str(i) for i in range(1,23)] + ['chrX']
vcfbub = config['vcfbub']


# filter out LV>0 bubbles
rule filter_bubbles:
	input:
		lambda wildcards: config['vcf'][wildcards.caller]
	output:
		vcf="results/data/vcf/{caller}-{threshold}/{caller}.vcf.gz"
	params:
		threshold = lambda wildcards: '-r ' + wildcards.threshold if wildcards.threshold != 'unfiltered' else ''
	wildcard_constraints:
		threshold = "unfiltered|10000|100000|1000000"
	resources:
		mem_total_mb=50000
	shell:
		"""
		{vcfbub} -l 0 {params.threshold} --input {input} | bgzip -c > {output.vcf}
		tabix -p vcf {output.vcf}
		"""

# remove sites that:
# - are covered by missing alleles in more than min_an haplotypes
# - are located outside of given chromosomes
# - contain Ns in sequence
# - are present only on CHM13
rule filter_vcf:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}.vcf.gz"
	output:
		temp("results/data/vcf/{caller}-{threshold}/{caller}_filtered.vcf")
	conda:
		"../envs/genotyping.yml"
	log:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered.log"
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=59
	params:
		sample=lambda wildcards: "chm13" if wildcards.caller == "pggb" else "CHM13"
	shell:
		"bcftools view --samples ^{params.sample} {input} | bcftools view --min-ac 1 | python3 workflow/scripts/filter-vcf.py {min_an} {min_an_male} {min_an_female} --chromosomes {chromosomes} 2> {log} 1> {output}"


# remove alternative alleles that are not covered by any haplotype
rule trim_alt_alleles:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered.vcf"
	output:
		temp("results/data/vcf/{caller}-{threshold}/{caller}_filtered_trim.vcf")
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=30
	shell:
		"bcftools view --trim-alt-alleles {input} > {output}"


# annotate the VCF and create an equivalent biallelic version of it
rule annotate_vcf:
	input:
		vcf="results/data/vcf/{caller}-{threshold}/{caller}_filtered_trim.vcf",
		gfa=lambda wildcards: config['gfa'][wildcards.caller]
	output:
		multi="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids.vcf",
		multi_tmp=temp("results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids-tmp.vcf"),
		biallelic="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf",
		bi_tmp=temp("results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids-tmp_biallelic.vcf")
	log:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids.log"
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=200000,
		runtime_hrs=5,
		runtime_min=59
	params:
		outname = "results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids-tmp"
	shell:
		"""
		python3 workflow/scripts/annotate_vcf.py -vcf {input.vcf} -gfa {input.gfa} -o {params.outname} &> {log}
		cat {output.multi_tmp} | awk '$1 ~ /^#/ {{print $0;next}} {{print $0 | \"sort -k1,1 -k2,2n\"}}' > {output.multi}
		cat {output.bi_tmp} | awk '$1 ~ /^#/ {{print $0;next}} {{print $0 | \"sort -k1,1 -k2,2n\"}}' > {output.biallelic}
		"""

# compress annotated
rule compress_annotated:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_{which}.vcf"
	output:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_{which}.vcf.gz"
	conda:
		"../envs/genotyping.yml"
	shell:
		"""
		bgzip -c {input} > {output}
		tabix -p vcf {output}
		"""


# determine untypable variant alleles per sample
rule untypable_ids:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf"
	output:
		lists=expand("results/data/vcf/{{caller}}-{{threshold}}/untypable-ids/{sample}-untypable.tsv", sample=[s for s in assembly_samples if s != 'CHM13']),
		summary="results/data/vcf/{caller}-{threshold}/untypable-ids.tsv"
	params:
		out="results/data/vcf/{caller}-{threshold}/untypable-ids"
	conda:
		"../envs/genotyping.yml"
	shell:
		"cat {input} | python3 workflow/scripts/untypable-ids.py {params.out} > {output.summary}"


# determine regions with complex variation (= multi-allelic bubbles)
rule alleles_per_bubble:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids.vcf"
	output:
		plot="results/data/vcf/{caller}-{threshold}/alleles-per-bubble.pdf",
		bed="results/data/vcf/{caller}-{threshold}/complex-bubbles.bed"
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=20000,
		runtime_hrs=1
	shell:
		"cat {input} | python3 workflow/scripts/variant-statistics.py {output.plot} 1 > {output.bed}"


# prepare bed file with repeats
# bed is downloaded already, sort and merge it
rule ucsc_repeats:
	input:
		"resources/ucsc-simple-repeats.bed"
	output:
		"results/data/bed/ucsc-simple-repeats.merged.bed"
	conda:
		"../envs/genotyping.yml"
	shell:
		"bedtools sort -i {input} | bedtools merge -i - > {output}"


# count bubbles and alleles
rule count_bubbles:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids.vcf"
	output:
		tsv="results/data/vcf/{caller}-{threshold}/{caller}_statistics.tsv",
		pdf="results/data/vcf/{caller}-{threshold}/{caller}_statistics.pdf"
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=10000
	shell:
		"cat {input} | python3 workflow/scripts/count-bubbles.py {output.pdf} > {output.tsv}"

# count alleles in bi/multiallelic regions
rule count_alleles_per_region:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids.vcf"
	output:
		"results/data/vcf/{caller}-{threshold}/{caller}_alleles-per-region.tsv"
	conda:
		"../envs/genotyping.yml"
	shell:
		"cat {input} | python3 workflow/scripts/count-bi-multi-alleles.py > {output}"
