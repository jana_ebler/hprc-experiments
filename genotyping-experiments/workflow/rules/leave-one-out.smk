configfile: "config/config.yaml"


sample_index_unrelated = config['sample_index_unrelated']
sample_index_related = config['sample_index_related']
read_dir_unrelated = config['read_dir_unrelated']
read_dir_related = config['read_dir_related']
pangenie = config['pangenie']

# read paths to sequencing data from files
samples = {}

# read 2504 unrelated samples
for line in open(sample_index_unrelated, 'r'):
	if line.startswith('study'):
		continue
	fields = line.split('\t')
	run_id = fields[3]
	sample_name = fields[2]
	if sample_name in config['assembly_samples']:
		samples[sample_name] = read_dir_unrelated + sample_name + '_' + run_id + '.fasta.gz'

# read 689 unrelated samples
for line in open(sample_index_related, 'r'):
	if line.startswith('study'):
		continue
	fields = line.split('\t')
	run_id = fields[3]
	sample_name = fields[2]
	if sample_name in config['assembly_samples']:
		samples[sample_name] = read_dir_related + sample_name + '_' + run_id + '.fasta.gz'

# consider all samples for which there are 1000G reads, skip haploid CHM13
leave_one_out_samples = [s for s in config['assembly_samples'] if (s != 'CHM13') and (s in samples)]

# used for testing different thresholds
leave_one_out_samples = ['HG00438', 'HG00733', 'HG02717', 'NA20129', 'HG03453']

variants = ['snp', 'indels', 'large-deletion', 'large-insertion', 'large-complex']

################################################################
######   prepare input panel and ground truth genotypes  #######
################################################################

# remove positions that are ".|." in the left out sample. These cannot be used for evaluation, as the true genotype is unknown
# for now, also remove CHM13, because PanGenie cannot handle haploid samples
# remove sample from the panel
rule remove_missing:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_{callset}.vcf"
	output:
		"results/leave-one-out/{caller}-{threshold}/{sample}/preprocessed-vcfs/{caller}-{threshold}_{callset}_no-missing.vcf"
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=20000
	priority: 1
	wildcard_constraints:
		callset = "filtered_ids|filtered_ids_biallelic"
	shell:
		"cat {input} | python3 workflow/scripts/remove-missing.py {wildcards.sample} > {output}"


rule prepare_panel:
	input:
		vcf="results/leave-one-out/{source}/{sample}/preprocessed-vcfs/{source}_filtered_ids_no-missing.vcf.gz"
	output:
		"results/leave-one-out/{source}/{sample}/input-panel/panel-{sample}.vcf"
	conda:
		"../envs/genotyping.yml"
	priority: 1
	log:
		"results/leave-one-out/{source}/{sample}/input-panel/panel-{sample}.log"
#	wildcard_constraints:
#		source = "cactus-unfiltered|cactus-10000|cactus-100000|cactus-1000000|pggb-unfiltered|pggb-10000|pggb-100000|pggb-1000000"
	resources:
		mem_total_mb=20000
	shell:
		"bcftools view --samples ^{wildcards.sample} {input.vcf} | bcftools view --min-ac 1 2> {log} 1> {output}"



# extract ground truth genotypes for sample
rule prepare_truth:
	input:
		vcf= "results/leave-one-out/{source}/{sample}/preprocessed-vcfs/{source}_filtered_ids_biallelic_no-missing.vcf.gz"
	output:
		"results/leave-one-out/{source}/{sample}/truth/truth-{sample}.vcf"
	conda:
		"../envs/genotyping.yml"
	priority: 1
	resources:
		mem_total_mb=20000
	log:
		"results/leave-one-out/{source}/{sample}/truth/truth-{sample}.log"
	shell:
		"bcftools view --samples {wildcards.sample} {input.vcf} 2> {log} 1> {output}"



########################################################
##################    run PanGenie    ##################
########################################################


# run pangenie
rule pangenie:
	input:
		reads= lambda wildcards: samples[wildcards.sample],
		fasta="results/data/fasta/hg38.fa",
		vcf="results/leave-one-out/{source}/{sample}/input-panel/panel-{sample}.vcf",
	output:
		reads = temp("results/leave-one-out/{source}/{sample}/pangenie/reads.fa"),
		path_segments = temp("results/leave-one-out/{source}/{sample}/pangenie/pangenie-{sample}_path_segments.fasta"),
		genotyping = "results/leave-one-out/{source}/{sample}/pangenie/pangenie-{sample}_genotyping.vcf"
	log:
		"results/leave-one-out/{source}/{sample}/pangenie/pangenie-{sample}.log"
	threads: 24
	resources:
		mem_total_mb=100000,
		runtime_hrs=3,
		runtime_min=1
	priority: 1
	params:
		out_prefix="results/leave-one-out/{source}/{sample}/pangenie/pangenie-{sample}"
	shell:
		"""
		gunzip -c {input.reads} > {output.reads}
		module load Singularity
		(/usr/bin/time -v {pangenie} -i {output.reads} -v {input.vcf} -r {input.fasta} -o {params.out_prefix} -s {wildcards.sample} -j {threads} -t {threads} -g ) &> {log}
		"""


########################################################
##################    Evaluation      ##################
########################################################

def region_to_bed(wildcards):
	if wildcards.regions == "easy":
		return "resources/GRCh38_notinalldifficultregions.bed"
	if wildcards.regions == "lowmap":
		return "resources/GRCh38_alllowmapandsegdupregions.bed"
	if wildcards.regions == "repeat":
		return "resources/GRCh38_AllTandemRepeats_gt100bp_slop5.bed"
	if wildcards.regions == "other":
		return "resources/GRCh38_allOtherDifficultregions.bed"
	if wildcards.regions == "biallelic":
		return "results/data/vcf/{source}/biallelic-bubbles.bed".format(source=wildcards.source)
	if wildcards.regions == "multiallelic":
		return "results/data/vcf/{source}/complex-bubbles.bed".format(source=wildcards.source)
	assert(False)



# prepare beds for biallelic and complex graph regions
rule prepare_beds:
	input:
		bed="results/data/vcf/{source}/complex-bubbles.bed",
		fai='results/data/fasta/hg38.fa.fai'
	output:
		bed="results/data/vcf/{source}/biallelic-bubbles.bed",
		tmp=temp("results/data/vcf/{source}/biallelic-bubbles.fai")
	conda:
		"../envs/genotyping.yml"
	shell:
		"""
		sort -k1,1d -k 2,2n -k 3,3n {input.fai} > {output.tmp}
		bedtools complement -i {input.bed} -g {output.tmp} > {output.bed}
		"""


# convert genotyped VCF to biallelic representation
rule convert_genotypes_to_biallelic:
	input:
		vcf="results/leave-one-out/{caller}-{threshold}/{sample}/pangenie/pangenie-{sample}_genotyping.vcf.gz",
		biallelic="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf"
	output:
		"results/leave-one-out/{caller}-{threshold}/{sample}/pangenie/pangenie-{sample}_genotyping-biallelic.vcf"
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=30000
	priority: 1
	shell:
		"zcat {input.vcf} | python3 workflow/scripts/convert-to-biallelic.py {input.biallelic} | awk '$1 ~ /^#/ {{print $0;next}} {{print $0 | \"sort -k1,1 -k2,2n \"}}' > {output}"


# determine untypable IDs
rule remove_untypable:
	input:
		vcf="results/leave-one-out/{source}/{sample}{other}.vcf",
		ids="results/data/vcf/{source}/untypable-ids/{sample}-untypable.tsv"
	output:
		vcf="results/leave-one-out/{source}/{sample}{other}-typable-{vartype}.vcf.gz",
		tbi="results/leave-one-out/{source}/{sample}{other}-typable-{vartype}.vcf.gz.tbi"
	wildcard_constraints:
#		source = "cactus-unfiltered|cactus-10000|cactus-100000|cactus-1000000|pggb-unfiltered|pggb-10000|pggb-100000|pggb-1000000", 
		sample="|".join(leave_one_out_samples),
		vartype="|".join(variants)
	resources:
		mem_total_mb=20000,
		runtime_hrs=1
	priority: 1
	shell:
		"""
		cat {input.vcf} | python3 workflow/scripts/skip-untypable.py {input.ids} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} | bgzip -c > {output.vcf}
		tabix -p vcf {output.vcf}
		"""

rule rtg_format:
	input:
		"results/data/fasta/hg38.fa"
	output:
		directory("results/leave-one-out/SDF")
	resources:
		mem_total_mb=20000
	priority: 1
	shell:
		'rtg format -o {output} {input}'


# precision-recall
rule vcfeval:
	input:
		callset="results/leave-one-out/{source}/{sample}/pangenie/pangenie-{sample}_genotyping-biallelic-typable-{vartype}.vcf.gz",
		callset_tbi="results/leave-one-out/{source}/{sample}/pangenie/pangenie-{sample}_genotyping-biallelic-typable-{vartype}.vcf.gz.tbi",
		baseline="results/leave-one-out/{source}/{sample}/truth/truth-{sample}-typable-{vartype}.vcf.gz",
		baseline_tbi="results/leave-one-out/{source}/{sample}/truth/truth-{sample}-typable-{vartype}.vcf.gz.tbi",
		regions= region_to_bed,
		sdf="results/leave-one-out/SDF"
	output:
		summary="results/leave-one-out/{source}/{sample}/evaluation/precision-recall-typable/pangenie/{regions}_{vartype}/summary.txt"
	conda:
		"../envs/genotyping.yml"
	priority: 1
	wildcard_constraints:
		sample = "|".join(samples),
		regions = "easy|lowmap|repeat|other|biallelic|multiallelic",
		vartype = "|".join(variants)
	params:
		tmp = "results/leave-one-out/{source}/{sample}/evaluation/precision-recall-typable/pangenie/{regions}_{vartype}_tmp",
		outname = "results/leave-one-out/{source}/{sample}/evaluation/precision-recall-typable/pangenie/{regions}_{vartype}",
		which = "--all-records"
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=40
	shell:
		"""
		rtg vcfeval -b {input.baseline} -c {input.callset} -t {input.sdf} -o {params.tmp} --ref-overlap --evaluation-regions {input.regions} {params.which} --Xmax-length 30000 > {output.summary}.tmp
		mv {params.tmp}/* {params.outname}/
		mv {output.summary}.tmp {output.summary}
		rm -r {params.tmp}
		"""


# determine the variants that went into re-typing per category
rule collected_typed_variants:
	input:
		callset="results/leave-one-out/{source}/{sample}/preprocessed-vcfs/{source}_filtered_ids_biallelic_no-missing.vcf.gz",
		regions= region_to_bed,
		ids="results/data/vcf/{source}/untypable-ids/{sample}-untypable.tsv"
	output:
		"results/leave-one-out/{source}/{sample}/genotyped-ids/{regions}_{vartype}.tsv"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		sample = "|".join(samples),
		regions = "easy|lowmap|repeat|other|biallelic|multiallelic",
		vartype = "|".join(variants)
	resources:
		mem_total_mb=50000
	priority: 1
	shell:
		"zcat {input.callset} | python3 workflow/scripts/skip-untypable.py {input.ids} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} | bedtools intersect -header -a - -b {input.regions} -u -f 0.5 | python3 workflow/scripts/get_ids.py > {output}"


# compute concordances
rule genotype_concordances:
	input:
		callset="results/leave-one-out/{source}/{sample}/pangenie/pangenie-{sample}_genotyping-biallelic-typable-{vartype}.vcf.gz",
		callset_tbi="results/leave-one-out/{source}/{sample}/pangenie/pangenie-{sample}_genotyping-biallelic-typable-{vartype}.vcf.gz.tbi",
		baseline="results/leave-one-out/{source}/{sample}/truth/truth-{sample}-typable-{vartype}.vcf.gz",
		baseline_tbi="results/leave-one-out/{source}/{sample}/truth/truth-{sample}-typable-{vartype}.vcf.gz.tbi",
		regions= region_to_bed,
		typed_ids = "results/leave-one-out/{source}/{sample}/genotyped-ids/{regions}_{vartype}.tsv"
	output:
		tmp_vcf1=temp("results/leave-one-out/{source}/{sample}/evaluation/concordance/pangenie_{regions}_{vartype}_base.vcf"),
		tmp_vcf2=temp("results/leave-one-out/{source}/{sample}/evaluation/concordance/pangenie_{regions}_{vartype}_call.vcf"),
		summary="results/leave-one-out/{source}/{sample}/evaluation/concordance/pangenie/{regions}_{vartype}/summary.txt"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		sample = "|".join(samples),
		regions = "easy|lowmap|repeat|other|biallelic|multiallelic",
		vartype = "|".join(variants)
	log:
		"results/leave-one-out/{source}/{sample}/evaluation/concordance/pangenie/{regions}_{vartype}/summary.log"
	resources:
		mem_total_mb=40000,
		runtime_hrs=0,
		runtime_min=40
	priority: 1
	shell:
		"""
		bedtools intersect -header -a {input.baseline} -b {input.regions} -u -f 0.5 | bgzip > {output.tmp_vcf1}
		bedtools intersect -header -a {input.callset} -b {input.regions} -u -f 0.5 | bgzip > {output.tmp_vcf2}
		python3 workflow/scripts/genotype-evaluation.py {output.tmp_vcf1} {output.tmp_vcf2} {input.typed_ids} --qual 0 2> {log} 1> {output.summary}
		"""


########################################################
##################     Plotting      ###################
########################################################

# collect results across all samples
rule collect_results:
	input:
		expand("results/leave-one-out/{{source}}/{sample}/evaluation/{{metric}}/pangenie/{{regions}}_{{vartype}}/summary.txt", sample = leave_one_out_samples)
	output:
		"results/leave-one-out/{source}/plots/{metric}/pangenie/{metric}_{regions}_{vartype}.tsv"
	params:
		samples = ','.join(leave_one_out_samples),
		outfile = 'results/leave-one-out/{source}/plots/{metric}/pangenie/{metric}'
	priority: 1
	shell:
		"python3 workflow/scripts/collect-results.py {wildcards.metric} {wildcards.source} {params.samples} {wildcards.regions} -variants {wildcards.vartype} -folder results/leave-one-out/ -outfile {params.outfile}"


# plot the results of the leave-one-out experiment (GIAB stratifications)
rule plotting_giab:
	input:
		expand("results/leave-one-out/{{source}}/plots/{{metric}}/pangenie/{{metric}}_{regions}_{vartype}.tsv", regions=["easy","lowmap","repeat","other"], vartype=variants)
	output:
		"results/leave-one-out/{source}/plots/{metric}/pangenie/{metric}_giab.pdf"
	wildcard_constraints:
		metric="concordance|precision-recall-typable",
#		source="cactus-unfiltered|cactus-10000|cactus-100000|cactus-1000000|pggb-unfiltered|pggb-10000|pggb-100000|pggb-1000000",
	priority: 1
	conda:
		"../envs/genotyping.yml"
	shell:
		"python3 workflow/scripts/plot-results.py -files {input} -outname {output} -sources easy lowmap repeat other -metric {wildcards.metric}"



# plot results using graph regions (biallelic/multiallelic) as stratifications
rule plotting_graph:
	input:
		expand("results/leave-one-out/{{source}}/plots/{{metric}}/pangenie/{{metric}}_{regions}_{vartype}.tsv", regions=["biallelic", "multiallelic"], vartype=variants)
	output:
		"results/leave-one-out/{source}/plots/{metric}/pangenie/{metric}_graph.pdf"
	wildcard_constraints:
		metric="concordance|precision-recall-typable",
#		source="cactus-unfiltered|cactus-10000|cactus-100000|cactus-1000000|pggb-unfiltered|pggb-10000|pggb-100000|pggb-1000000",
	priority: 1
	conda:
		"../envs/genotyping.yml"
	shell:
		"python3 workflow/scripts/plot-results.py -files {input} -outname {output} -sources biallelic multiallelic -metric {wildcards.metric}"



rule compress_vcf:
	input:
		"results/leave-one-out/{filename}.vcf"
	output:
		vcf="results/leave-one-out/{filename}.vcf.gz",
		tbi="results/leave-one-out/{filename}.vcf.gz.tbi"
	priority: 1
	shell:
		"""
		bgzip -c {input} > {output.vcf}
		tabix -p vcf {output.vcf}
		"""
