configfile: "config/config.yaml"

truthset_to_sample = {
	"giab-med": "NA24385",
	"dipcall": "NA24385",
	"giab-dipcall": "NA24385",
	"giab-high": "NA24385"
}

region_to_bed_dict = {
	"easy": "resources/GRCh38_notinalldifficultregions.bed",
	"lowmap": "resources/GRCh38_alllowmapandsegdupregions.bed",
	"other": "resources/GRCh38_allOtherDifficultregions.bed",
	"repeat": "resources/GRCh38_AllTandemRepeats_gt100bp_slop5.bed"
}


rule download_giab_high_conf:
	output:
		vcf="results/data/vcf/giab-high/giab-high.vcf.gz",
		tbi="results/data/vcf/giab-high/giab-high.vcf.gz.tbi",
		bed="results/data/vcf/giab-high/giab-high.bed"
	shell:
		"""
		wget -O {output.vcf} https://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/HG002_NA24385_son/NISTv4.2.1/GRCh38/HG002_GRCh38_1_22_v4.2.1_benchmark.vcf.gz 
		wget -O {output.tbi} https://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/HG002_NA24385_son/NISTv4.2.1/GRCh38/HG002_GRCh38_1_22_v4.2.1_benchmark.vcf.gz.tbi
		wget -O {output.bed} https://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/AshkenazimTrio/HG002_NA24385_son/NISTv4.2.1/GRCh38/HG002_GRCh38_1_22_v4.2.1_benchmark_noinconsistent.bed
		"""

rule download_giab_med_svs:
	output:
		vcf="results/data/vcf/giab-med/giab-med.vcf.gz",
		tbi="results/data/vcf/giab-med/giab-med.gz.tbi",
		bed="results/data/vcf/giab-med/giab-med.bed"
	shell:
		"""
		wget -O {output.vcf} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/release/AshkenazimTrio/HG002_NA24385_son/CMRG_v1.00/GRCh38/StructuralVariant/HG002_GRCh38_CMRG_SV_v1.00.vcf.gz
		wget -O {output.tbi} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/release/AshkenazimTrio/HG002_NA24385_son/CMRG_v1.00/GRCh38/StructuralVariant/HG002_GRCh38_CMRG_SV_v1.00.vcf.gz.tbi
		wget -O {output.bed} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/release/AshkenazimTrio/HG002_NA24385_son/CMRG_v1.00/GRCh38/StructuralVariant/HG002_GRCh38_CMRG_SV_v1.00.bed
		"""

# there are some entries like chr14_GL000194v1_random in the BED which do not occur in the VCF header (and records). truvari seems to break because of that
# therefore, these entries are removed. 
rule download_dicall:
	output:
		vcf="results/data/vcf/dipcall/dipcall.vcf.gz",
		tbi="results/data/vcf/dipcall/dipcall.gz.tbi",
		bed="results/data/vcf/dipcall/dipcall.bed",
		tmp=temp("results/data/vcf/dipcall/dipcall-tmp.bed")
	shell:
		"""
		wget -O {output.vcf} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/data/AshkenazimTrio/analysis/HPRC-HG002.cur.20211005/HPRC-cur.20211005-align2-GRCh38.dip.vcf.gz
		wget -O {output.tbi} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/data/AshkenazimTrio/analysis/HPRC-HG002.cur.20211005/HPRC-cur.20211005-align2-GRCh38.dip.vcf.gz.tbi
		wget -O {output.tmp} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/data/AshkenazimTrio/analysis/HPRC-HG002.cur.20211005/HPRC-cur.20211005-align2-GRCh38.dip.bed
		grep -v "_" {output.tmp} > {output.bed}
		"""


# download link unknown..
rule prepare_dipcall_small_variant_benchmark:
	input:
		vcf="resources/GRCh38_HG2-HPRC-20211005_dipcall-z2k.vcf.gz",
		tbi="resources/GRCh38_HG2-HPRC-20211005_dipcall-z2k.vcf.gz.tbi",
		bed="resources/GRCh38_HG2-HPRC-20211005_dipcall-z2k.excluded.bed"
	output:
		vcf="results/data/vcf/giab-dipcall/giab-dipcall.vcf.gz",
		tbi="results/data/vcf/giab-dipcall/giab-dipcall.vcf.gz.tbi",
		bed="results/data/vcf/giab-dipcall/giab-dipcall.bed"
	shell:
		"""
		cp {input.vcf} {output.vcf}
		cp {input.tbi} {output.tbi}
		cp {input.bed} {output.bed}
		"""


####################################################################################################
#  find out which variants in the truth set are not contained in the pangenome graph (=untypables)
####################################################################################################


# assign each variant a unique ID (if a variant matches with panel, use the same ID as panel)
rule annotate_variants_callset:
	input:
		callset="results/data/vcf/{truthset}/{truthset}.vcf.gz",
		panel="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf"
	output:
		"results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
	wildcard_constraints:
		truthset = "|".join([t for t in truthset_to_sample.keys()])
	resources:
		mem_total_mb=20000,
		runtime_hrs=1,
		runtime_min=59
	conda:
		"../envs/genotyping.yml"
	shell:
		"""
		bcftools norm -m -any {input.callset} | python3 workflow/scripts/annotate.py {input.panel} | bgzip -c > {output}
		tabix -p vcf {output}
		"""



# for each panel sample determine its false negatives comparing to the truth set.
# these are variants only in the truth, but not detected in the sample itself.
# later, intersect all false negatives across the panel samples, to find out
# which variants are not present in the pangenome graph, but are in the truth.
# these variants are not accessible by re-genotyping methods, that are unable
# to detect variants themselves ("untypables").
rule determine_false_negatives_truvari:
	input:
		truthset="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
		panel="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf.gz",
		reference="results/data/fasta/hg38.fa",
		ref_index="results/data/fasta/hg38.fa.fai"
	output:
		sample_vcf="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}-truvari.vcf.gz",
		fn="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/truvari/fn.vcf.gz"
	wildcard_constraints:
		truthset="|".join([t for t in truthset_to_sample.keys()])
	conda:
		"../envs/genotyping.yml"
	params:
		tmp="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/truvari_temp",
		outname="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/truvari"
	resources:
		mem_total_mb=30000,
		runtime_hrs=0,
		runtime_min=59
	log:
		"results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/truvari.log"
	shell:
		"""
		bcftools view --samples {wildcards.sample} {input.panel} | bcftools view --min-ac 1 | python3 workflow/scripts/prepare-for-truvari.py {input.ref_index} | bgzip -c > {output.sample_vcf}
		tabix -p vcf {output.sample_vcf}
		truvari bench -b {input.truthset} -c {output.sample_vcf} -f {input.reference} -o {params.tmp} --multimatch -r 2000 --no-ref a -C 2000 --passonly &> {log}
		bgzip {params.tmp}/fn.vcf
		tabix -p vcf {params.tmp}/fn.vcf.gz
		mv {params.tmp}/* {params.outname}/
		rm -r {params.tmp}
		"""


rule determine_false_negatives_vcfeval:
	input:
		truthset="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
		panel="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf.gz",
		reference="results/data/fasta/hg38.fa",
		ref_index="results/data/fasta/hg38.fa.fai",
		sdf="results/data/fasta/SDF"
	output:
		sample_vcf="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}-vcfeval.vcf.gz",
		fn="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/vcfeval/fn.vcf.gz"
	wildcard_constraints:
		truthset="|".join([t for t in truthset_to_sample.keys()])
	conda:
		"../envs/genotyping.yml"
	params:
		tmp="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/vcfeval_temp",
		outname="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/vcfeval"
	resources:
		mem_total_mb=30000,
		runtime_hrs=0,
		runtime_min=59
	log:
		"results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/vcfeval.log"
	shell:
		"""
		bcftools view --samples {wildcards.sample} {input.panel} | bcftools view --min-ac 1 | python3 workflow/scripts/prepare-for-truvari.py {input.ref_index} | bgzip -c > {output.sample_vcf}
		tabix -p vcf {output.sample_vcf}
		rtg vcfeval -b {input.truthset} -c {output.sample_vcf} -t {input.sdf} -o {params.tmp} --squash-ploidy  &> {log}
		mv {params.tmp}/* {params.outname}/
		rm -r {params.tmp}
		"""



rule determine_unique:
	input:
		truthset="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
		samples = expand("results/data/vcf/{{truthset}}/{{caller}}-{{threshold}}/samples/{sample}/{{method}}/fn.vcf.gz", sample=[s for s in config["assembly_samples"] if not s == "CHM13"])
	output:
		unique_tsv="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique_{method}.tsv",
		unique_vcf="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique_{method}.vcf"
	conda:
		"../envs/genotyping.yml"
	params:
		n_files = len([s for s in config['assembly_samples'] if not s == "CHM13"])
	resources:
		mem_total_mb=30000,
		runtime_hrs=0,
		runtime_min=30
	wildcard_constraints:
		method = "truvari|vcfeval"
	shell:
		"""
		bcftools isec -n={params.n_files} -w1 {input.samples}  > {output.unique_vcf}
		grep -v '#' {output.unique_vcf} | cut -f 3  > {output.unique_tsv}
		"""

rule count_unique_type:
	input:
		total="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
		unique="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique_{method}.vcf"
	output:
		total="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-total_{method}-{vartype}.vcf",
		unique="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique_{method}-{vartype}.vcf"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		vartype="indel|sv",
		method="vcfeval|truvari"
	shell:
		"""
		bcftools view {input.total} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} > {output.total}
		bcftools view {input.unique} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} > {output.unique}
		"""




####################################################################################################
#  compare genotyping results to the truthset, excluding untypable variants which cannot be 
# genotyped correctly by a re-genotyper
####################################################################################################

# create pangenie callset containing only variants that made it into the lenient set
rule extract_filtered_set:
	input:
		vcf="results/population-typing/{source}/genotyping/{sample}_genotyping_bi_all.vcf.gz",
		filters="results/population-typing/{source}/evaluation/statistics/plot_bi_all_filters.tsv"
	output:
		"results/callset-comparisons/callsets/pangenie/{source}/{sample}_lenient.vcf.gz"
	resources:
		mem_total_mb=10000,
		runtime_hrs=1,
		runtime_min=59
	shell:
		"""
		zcat {input.vcf} | python3 workflow/scripts/select_ids.py {input.filters} lenient | bgzip -c > {output}
		tabix -p vcf {output}
		"""


# remove untypables and extract only variants of a specific type
def input_extract_variant_type_callset(wildcards):
	if wildcards.set == "pangenie-unfiltered":
		return "results/population-typing/{caller}-{threshold}/genotyping/{sample}_genotyping_bi_all.vcf.gz".format(caller= wildcards.caller, threshold=wildcards.threshold, sample = truthset_to_sample[wildcards.truthset])
	elif wildcards.set == "pangenie-lenient":
		return "results/callset-comparisons/callsets/pangenie/{caller}-{threshold}/{sample}_lenient.vcf.gz".format(caller= wildcards.caller, threshold=wildcards.threshold, sample = truthset_to_sample[wildcards.truthset])
	else:
		assert wildcards.set in truthset_to_sample.keys()
		return "results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz".format(truthset=wildcards.truthset, caller=wildcards.caller, threshold=wildcards.threshold)


rule extract_variant_type_callset:
	input:
		vcf= input_extract_variant_type_callset,
		untypable="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique_{method}.tsv"
	output:
		vcf="results/callset-comparisons/{truthset}/{caller}-{threshold}/{set}-typable-{vartype}_{method}.vcf.gz",
		tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{set}-typable-{vartype}_{method}.vcf.gz.tbi"
	wildcard_constraints:
		truthset = "|".join([t for t in truthset_to_sample.keys()]),
		set= "|".join([t for t in truthset_to_sample.keys()] + ["pangenie-unfiltered", "pangenie-lenient"]),
		vartype="sv|indel",
		method = "truvari|vcfeval"
	resources:
		mem_total_mb=20000
	shell:
		"""
		zcat {input.vcf} | python3 workflow/scripts/skip-untypable.py {input.untypable} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} | bgzip -c > {output.vcf}
		tabix -p vcf {output.vcf}
		"""

# keep untypables and only variants of a specific type
rule extract_variant_type_callset_all: 
	input:
		vcf= input_extract_variant_type_callset
	output:
		vcf="results/callset-comparisons/{truthset}/{caller}-{threshold}/{set}-all-{vartype}_{method}.vcf.gz",
		tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{set}-all-{vartype}_{method}.vcf.gz.tbi"
	wildcard_constraints:
		truthset = "|".join([t for t in truthset_to_sample.keys()]),
		set= "|".join([t for t in truthset_to_sample.keys()] + ["pangenie-unfiltered", "pangenie-lenient"]),
		vartype="sv|indel",
		method = "truvari|vcfeval"
	resources:
		mem_total_mb=20000
	shell:
		"""
		zcat {input.vcf} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} | bgzip -c > {output.vcf}
		tabix -p vcf {output.vcf}
		"""


# prepare regions (stratification BED files intersected with callable regions as defined by the BED files that come with each ground truth)
rule prepare_evaluation_beds:
	input:
		callable_regions="results/data/vcf/{truthset}/{truthset}.bed",
		bed= lambda wildcards: region_to_bed_dict[wildcards.region] if wildcards.region != 'all' else "results/data/vcf/{truthset}/{truthset}.bed"
	output:
		"results/callset-comparisons/{truthset}/bed-files/{truthset}_{region}.bed"
	wildcard_constraints:
		truthset = "|".join([t for t in truthset_to_sample.keys()]),
		region = "all|easy|lowmap|other|repeat"
	resources:
		mem_total_mb=20000,
		runtime_hrs=1
	conda:
		"../envs/genotyping.yml"
	shell:
		"bedtools intersect -a {input.callable_regions} -b {input.bed} > {output}"



# compute precision/recall for SVs
rule truvari_callsets:
	input:
		callset="results/callset-comparisons/{truthset}/{caller}-{threshold}/{callset}-{filter}-{vartype}_truvari.vcf.gz",
		callset_tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{callset}-{filter}-{vartype}_truvari.vcf.gz.tbi",
		baseline="results/callset-comparisons/{truthset}/{caller}-{threshold}/{truthset}-{filter}-{vartype}_truvari.vcf.gz",
		baseline_tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{truthset}-{filter}-{vartype}_truvari.vcf.gz.tbi",
		regions="results/callset-comparisons/{truthset}/bed-files/{truthset}_{region}.bed",
		reference="results/data/fasta/hg38.fa",
		ref_index="results/data/fasta/hg38.fa.fai"
	output:
		fixed_vcf=temp("results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_{filter}_region-{region}_fixed.vcf.gz"),
		fixed_vcf_tbi=temp("results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_{filter}_region-{region}_fixed.vcf.gz.tbi"),
		summary="results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_{filter}_region-{region}/summary.txt"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		vartype = "sv",
		filter = "all|typable"
	log:
		"results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_{filter}_region-{region}.log"
	params:
		tmp = "results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_{filter}_region-{region}_tmp",
		outname = "results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_{filter}_region-{region}",
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=40
	shell:
		"""
		bcftools view {input.callset} --min-ac 1 | python3 workflow/scripts/prepare-for-truvari.py {input.ref_index} | bgzip -c > {output.fixed_vcf}
		tabix -p vcf {output.fixed_vcf}

		truvari bench -b {input.baseline} -c {output.fixed_vcf} -f {input.reference} -o {params.tmp} --multimatch --includebed {input.regions} -r 2000 --no-ref a -C 2000 --passonly &> {log}
		mv {params.tmp}/* {params.outname}/
		rm -r {params.tmp}
		"""

rule rtg_format_callsets:
	input:
		"results/data/fasta/hg38.fa"
	output:
		directory("results/data/fasta/SDF")
	resources:
		mem_total_mb=20000
	conda:
		"../envs/genotyping.yml"
	shell:
		"rtg format -o {output} {input}"


# compute precision/recall for small variants
rule vcfeval_callsets:
	input:
		callset="results/callset-comparisons/{truthset}/{caller}-{threshold}/{callset}-{filter}-{vartype}_vcfeval.vcf.gz",
		callset_tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{callset}-{filter}-{vartype}_vcfeval.vcf.gz.tbi",
		baseline="results/callset-comparisons/{truthset}/{caller}-{threshold}/{truthset}-{filter}-{vartype}_vcfeval.vcf.gz",
		baseline_tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{truthset}-{filter}-{vartype}_vcfeval.vcf.gz.tbi",
		regions= "results/callset-comparisons/{truthset}/bed-files/{truthset}_{region}.bed",
		reference="results/data/fasta/hg38.fa",
		ref_index = "results/data/fasta/hg38.fa.fai",
		sdf="results/data/fasta/SDF"
	output:
		fixed_vcf=temp("results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_{filter}_region-{region}_fixed.vcf.gz"),
		fixed_vcf_tbi=temp("results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_{filter}_region-{region}_fixed.vcf.gz.tbi"),
		summary="results/callset-comparisons/{truthset}/{caller}-{threshold}/vcfeval_{callset}_{vartype}_{filter}_region-{region}/summary.txt"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		vartype = "indel",
		filter = "all|typable"
	log:
		"results/callset-comparisons/{truthset}/{caller}-{threshold}/vcfeval_{callset}_{vartype}_{filter}_region-{region}.log"
	params:
		tmp = "results/callset-comparisons/{truthset}/{caller}-{threshold}/vcfeval_{callset}_{vartype}_{filter}_region-{region}_tmp",
		outname = "results/callset-comparisons/{truthset}/{caller}-{threshold}/vcfeval_{callset}_{vartype}_{filter}_region-{region}"
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=40
	shell:
                """
		bcftools view {input.callset} --min-ac 1 | python3 workflow/scripts/prepare-for-truvari.py {input.ref_index} | bgzip -c > {output.fixed_vcf}
		tabix -p vcf {output.fixed_vcf}

		rtg vcfeval -b {input.baseline} -c {output.fixed_vcf} -t {input.sdf} -o {params.tmp} --evaluation-regions {input.regions} > {output.summary}.tmp
		mv {params.tmp}/* {params.outname}/
		mv {output.summary}.tmp {output.summary}
		rm -r {params.tmp}
                """
