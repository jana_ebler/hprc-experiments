configfile: "config/config.yaml"

truthset_to_sample = {
	"giab-med": "NA24385"
}



rule download_giab_med_svs:
	output:
		vcf="results/data/vcf/giab-med/giab-med.vcf.gz",
		tbi="results/data/vcf/giab-med/giab-med.gz.tbi",
		bed="results/data/vcf/giab-med/giab-med.bed"
	shell:
		"""
		wget -O {output.vcf} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/release/AshkenazimTrio/HG002_NA24385_son/CMRG_v1.00/GRCh38/StructuralVariant/HG002_GRCh38_CMRG_SV_v1.00.vcf.gz
		wget -O {output.tbi} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/release/AshkenazimTrio/HG002_NA24385_son/CMRG_v1.00/GRCh38/StructuralVariant/HG002_GRCh38_CMRG_SV_v1.00.vcf.gz.tbi
		wget -O {output.bed} https://ftp-trace.ncbi.nlm.nih.gov/ReferenceSamples/giab/release/AshkenazimTrio/HG002_NA24385_son/CMRG_v1.00/GRCh38/StructuralVariant/HG002_GRCh38_CMRG_SV_v1.00.bed
		"""



####################################################################################################
#  find out which variants in the truth set are not contained in the pangenome graph (=untypables)
####################################################################################################


# assign each variant a unique ID (if a variant matches with panel, use the same ID as panel)
rule annotate_variants_callset:
	input:
		callset="results/data/vcf/{truthset}/{truthset}.vcf.gz",
		panel="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf"
	output:
		"results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
	wildcard_constraints:
		truthset = "|".join([t for t in truthset_to_sample.keys()])
	resources:
		mem_total_mb=20000,
		runtime_hrs=1,
		runtime_min=59
	conda:
		"../envs/genotyping.yml"
	shell:
		"""
		bcftools norm -m -any {input.callset} | python3 workflow/scripts/annotate.py {input.panel} | bgzip -c > {output}
		tabix -p vcf {output}
		"""



# for each panel sample determine its false negatives comparing to the truth set.
# these are variants only in the truth, but not detected in the sample itself.
# later, intersect all false negatives across the panel samples, to find out
# which variants are not present in the pangenome graph, but are in the truth.
# these variants are not accessible by re-genotyping methods, that are unable
# to detect variants themselves ("untypables").
rule determine_false_negatives:
	input:
		truthset="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
		panel="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf.gz",
		reference="results/data/fasta/hg38.fa",
		ref_index="results/data/fasta/hg38.fa.fai"
	output:
		sample_vcf="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}.vcf.gz",
		fn="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/truvari/fn.vcf.gz"
	wildcard_constraints:
		truthset="|".join([t for t in truthset_to_sample.keys()])
	conda:
		"../envs/genotyping.yml"
	params:
		tmp="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/truvari_temp",
		outname="results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/truvari",
		sdf= "results/data/vcf/{truthset}/SDF"
	resources:
		mem_total_mb=30000,
		runtime_hrs=0,
		runtime_min=59
	log:
		"results/data/vcf/{truthset}/{caller}-{threshold}/samples/{sample}/truvari.log"
	shell:
		"""
		bcftools view --samples {wildcards.sample} {input.panel} | bcftools view --min-ac 1 | python3 workflow/scripts/prepare-for-truvari.py {input.ref_index} | bgzip -c > {output.sample_vcf}
		tabix -p vcf {output.sample_vcf}
		truvari bench -b {input.truthset} -c {output.sample_vcf} -f {input.reference} -o {params.tmp} --multimatch -r 2000 &> {log}
		bgzip {params.tmp}/fn.vcf
		tabix -p vcf {params.tmp}/fn.vcf.gz
		mv {params.tmp}/* {params.outname}/
		rm -r {params.tmp}
		"""

rule determine_unique:
	input:
		truthset="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
		samples = expand("results/data/vcf/{{truthset}}/{{caller}}-{{threshold}}/samples/{sample}/truvari/fn.vcf.gz", sample=[s for s in config["assembly_samples"] if not s == "CHM13"])
	output:
		unique_tsv="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique.tsv",
		unique_vcf="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique.vcf"
	conda:
		"../envs/genotyping.yml"
	params:
		n_files = len([s for s in config['assembly_samples'] if not s == "CHM13"])
	resources:
		mem_total_mb=30000,
		runtime_hrs=0,
		runtime_min=30
	shell:
		"""
		bcftools isec -n={params.n_files} -w1 {input.samples}  > {output.unique_vcf}
		grep -v '#' {output.unique_vcf} | cut -f 3  > {output.unique_tsv}
		"""

rule count_unique_type:
	input:
		total="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz",
		unique="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique.vcf"
	output:
		total="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-total-{vartype}.vcf",
		unique="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique-{vartype}.vcf"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		vartype="indel|sv"
	shell:
		"""
		bcftools view {input.total} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} > {output.total}
		bcftools view {input.unique} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} > {output.unique}
		"""




####################################################################################################
#  compare genotyping results to the truthset, excluding untypable variants which cannot be 
# genotyped correctly by a re-genotyper
####################################################################################################




# remove untypables and extract only variants of a specific type
def input_extract_variant_type_callset(wildcards):
	if wildcards.set == "pangenie":
		return "results/population-typing/{caller}-{threshold}/genotyping/{sample}_genotyping_bi_all.vcf.gz".format(caller= wildcards.caller, threshold=wildcards.threshold, sample = truthset_to_sample[wildcards.truthset])
	else:
		assert wildcards.set in truthset_to_sample.keys()
		return "results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-annotated.vcf.gz".format(truthset=wildcards.truthset, caller=wildcards.caller, threshold=wildcards.threshold)

rule extract_variant_type_callset:
	input:
		vcf= input_extract_variant_type_callset,
		untypable="results/data/vcf/{truthset}/{caller}-{threshold}/{truthset}-unique.tsv"
	output:
		vcf="results/callset-comparisons/{truthset}/{caller}-{threshold}/{set}-typable-{vartype}.vcf.gz",
		tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{set}-typable-{vartype}.vcf.gz.tbi"
	wildcard_constraints:
		truthset = "|".join([t for t in truthset_to_sample.keys()]),
		set= "|".join([t for t in truthset_to_sample.keys()] + ["pangenie"]),
		vartype="sv"
	resources:
		mem_total_mb=20000
	shell:
		"""
		zcat {input.vcf} | python3 workflow/scripts/skip-untypable.py {input.untypable} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} | bgzip -c > {output.vcf}
		tabix -p vcf {output.vcf}
		"""


# compute precision/recall
rule truvari_callsets:
	input:
		callset="results/callset-comparisons/{truthset}/{caller}-{threshold}/{callset}-typable-{vartype}.vcf.gz",
		callset_tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{callset}-typable-{vartype}.vcf.gz.tbi",
		baseline="results/callset-comparisons/{truthset}/{caller}-{threshold}/{truthset}-typable-{vartype}.vcf.gz",
		baseline_tbi="results/callset-comparisons/{truthset}/{caller}-{threshold}/{truthset}-typable-{vartype}.vcf.gz.tbi",
		regions="results/data/vcf/{truthset}/{truthset}.bed",
		reference="results/data/fasta/hg38.fa",
		ref_index="results/data/fasta/hg38.fa.fai"
	output:
		fixed_vcf=temp("results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_fixed.vcf.gz"),
		fixed_vcf_tbi=temp("results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_fixed.vcf.gz.tbi"),
		summary="results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}/summary.txt"
	conda:
		"../envs/genotyping.yml" # TODO: add bcftools to this env
	wildcard_constraints:
		vartype = "sv"
	log:
		"results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}.log"
	params:
		tmp = "results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}_tmp",
		outname = "results/callset-comparisons/{truthset}/{caller}-{threshold}/truvari_{callset}_{vartype}",
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=40
	shell:
		"""
		bcftools view {input.callset} --min-ac 1 | python3 workflow/scripts/prepare-for-truvari.py {input.ref_index} | bgzip -c > {output.fixed_vcf}
		tabix -p vcf {output.fixed_vcf}

		truvari bench -b {input.baseline} -c {output.fixed_vcf} -f {input.reference} -o {params.tmp} --multimatch --includebed {input.regions} -r 2000 &> {log}
		mv {params.tmp}/* {params.outname}/
		rm -r {params.tmp}
		"""
