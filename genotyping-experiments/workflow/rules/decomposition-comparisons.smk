configfile: "config/config.yaml"

snarl_decomp = config['snarl_decomp']

rule prepare_input:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}.vcf.gz"
	output:
		"results/decompositions/{caller}-{threshold}/snarls/{caller}-{threshold}.vcf.gz"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		threshold = "unfiltered"
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=59
	shell:
		"""
		bcftools view --samples HG00438 {input} | python3 workflow/scripts/decomposition-helper.py prepare_input | bgzip -c > {output}
		tabix -p vcf {output}
		"""

rule run_snarl_decomposition:
	input:
		vcf=lambda wildcards: config['vcf'][wildcards.caller],
		genotypes="results/decompositions/{caller}-{threshold}/snarls/{caller}-{threshold}.vcf.gz"
	output:
		"results/decompositions/{caller}-{threshold}/snarls/{caller}-{threshold}-annotated.vcf"
	threads: 24
	resources:
		mem_total_mb=100000,
		runtime_hrs=12,
		runtime_min=59
	shell:
		"{snarl_decomp} {input.vcf} {input.genotypes} -t {threads} > {output}"


rule filter_decomposition:
	input:
		traversal="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids.vcf",
		snarl="results/decompositions/{caller}-{threshold}/snarls/{caller}-{threshold}-annotated.vcf"
	output:
		"results/decompositions/{caller}-{threshold}/snarls/{caller}-{threshold}-annotated-filtered.vcf"
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=59
	shell:
		"python3 workflow/scripts/decomposition-helper.py filter -traversal {input.traversal} -snarls {input.snarl} > {output}"


rule compute_decomposition_stats:
	input:
		"results/decompositions/{caller}/snarls/{caller}-annotated-filtered.vcf"
	output:
		"results/decompositions/{caller}/snarls/{caller}-annotated-filtered.tsv"
	resources:
		mem_total_mb=20000,
		runtime_hrs=0,
		runtime_min=59
	shell:
		"python3 workflow/scripts/decomposition-helper.py count -vcf {input} > {output}"
