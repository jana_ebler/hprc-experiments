configfile: "config/config.yaml"

callset_to_vcf = {
	"hgsvc": config['hgsvc_genotypes'],
	"hprc": "results/population-typing/{source}/merged-vcfs/whole-genome/all-samples_bi_all.vcf.gz",
	"illumina": config['illumina']
}

sample_to_panel = {
	"full" : "",
	"HG01083" : "",
	"NA12878" : "in_HGSVC-panel",
	"HG00438" : "in_HPRC-panel"
}

callset_panel_samples = {
	"hprc" : [s for s in config['assembly_samples']],
	"hgsvc": [s for s in config['hgsvc_samples']]
}


## Note: this pipeline considers only chromosomes 1-22 + chr X

# also determine the list of samples contained in each VCF
rule prepare_vcf:
	input:
		lambda wildcards: callset_to_vcf[wildcards.callset]
	output:
		samples = "results/population-typing/{source}/evaluation/plots/vcfs/{callset}_all.tsv"
	wildcard_constraints:
		callset = 'hprc|hgsvc|illumina'
	log:
		"results/population-typing/{source}/evaluation/plots/vcfs/{callset}_all.log"
	conda:
		"../envs/whatshap.yml"
	shell:
		"""
		bcftools query -l {input} > {output.samples}
		"""

# find out which samples are contained in HPRC/HGSVC vcfs
rule intersect_samples:
	input:
		samples = expand("results/population-typing/{{source}}/evaluation/plots/vcfs/{callset}_all.tsv", callset=['hgsvc', 'hprc', 'illumina'])
	output:
		"results/population-typing/{source}/evaluation/plots/vcfs/samples-intersection.tsv"
	run:
		result = None
		for sample in input.samples:
			names = set([s.strip() for s in open(sample, 'r')])
			if result is None:
				result = names
			else:
				result = result & names
		with open(output[0], 'w') as outfile:
			for s in result:
				outfile.write(s + '\n')


# keep only intersection of samples (so that VCFs are comparable)
# add AF,AN,AC tags to VCFs (to make sure all VCFs have them). HPRC variants first need to be merged with truvari
rule extract_intersection_samples_collapse:
	input:
		vcf= lambda wildcards: callset_to_vcf['hprc'],
		samples = "results/population-typing/{source}/evaluation/plots/vcfs/samples-intersection.tsv",
		reference = "results/data/fasta/hg38.fa"
	output:
		unmerged=temp("results/population-typing/{source}/evaluation/plots/vcfs/tmp-hprc_intersection_all_full-unmerged.vcf.gz"),
		merged="results/population-typing/{source}/evaluation/plots/vcfs/hprc_intersection_all_full_collapsed.vcf.gz"
	conda:
		"../envs/whatshap.yml"
	resources:
		mem_total_mb=80000,
		runtime_hrs=23,
		runtime_min=59
	shell:
		"""
		bcftools view --samples-file {input.samples} {input.vcf} | python3 workflow/scripts/extract-varianttype.py large | bgzip -c > {output.unmerged}
		tabix -p vcf {output.unmerged}
		truvari collapse -r 500 -p 0.95 -P 0.95 -s 50 -S 100000 -f {input.reference} -i {output.unmerged} | bcftools sort | bcftools +fill-tags -Oz -o {output.merged} -- -t AN,AC,AF
		tabix -p vcf {output.merged}
		"""


# select high conf variants from HPRC after intersection of variants. HPRC variants first need to be merged with truvari
rule produce_filtered_callsets_collapse:
	input:
		vcf="results/population-typing/{source}/evaluation/plots/vcfs/tmp-hprc_intersection_all_full-unmerged.vcf.gz",
		filters= "results/population-typing/{source}/evaluation/statistics/plot_bi_all_filters.tsv",
		reference = "results/data/fasta/hg38.fa"
	output:
		unmerged=temp("results/population-typing/{source}/evaluation/plots/vcfs/tmp-hprc_intersection_{filter}_full-unmerged.vcf.gz"),
		merged="results/population-typing/{source}/evaluation/plots/vcfs/hprc_intersection_{filter}_full_collapsed.vcf.gz"
	resources:
		mem_total_mb=80000,
		runtime_hrs=5,
		runtime_min=59
	conda:
		"../envs/whatshap.yml"
	wildcard_constraints:
		filter = 'strict|lenient'
	shell:
		"""
		zcat {input.vcf} | python3 workflow/scripts/select_ids.py {input.filters} {wildcards.filter} | bgzip -c > {output.unmerged}
		tabix -p vcf {output.unmerged}
		truvari collapse -r 500 -p 0.95 -P 0.95 -s 50 -S 100000 -f {input.reference} -i {output.unmerged} | bcftools sort | bcftools +fill-tags -Oz -o {output.merged} -- -t AN,AC,AF
		tabix -p vcf {output.merged}
		"""


# keep only intersection of samples (so that VCFs are comparable)
# add AF,AN,AC tags to VCFs (to make sure all VCFs have them)
rule extract_intersection_samples:
	input:
		vcf= lambda wildcards: callset_to_vcf[wildcards.callset],
		samples = "results/population-typing/{source}/evaluation/plots/vcfs/samples-intersection.tsv"
	output:
		"results/population-typing/{source}/evaluation/plots/vcfs/{callset}_intersection_all_full_raw.vcf.gz"
	conda:
		"../envs/whatshap.yml"
	wildcard_constraints:
		callset = "hprc|hgsvc|illumina"
	resources:
		mem_total_mb=80000,
		runtime_hrs=23,
		runtime_min=59
	shell:
		"""
		bcftools view --samples-file {input.samples} {input.vcf} | python3 workflow/scripts/extract-varianttype.py large | bcftools +fill-tags -Oz -o {output} -- -t AN,AC,AF
		tabix -p vcf {output}
		"""


# select high conf variants from HGSVC after intersection of variants
rule produce_filtered_callsets:
	input:
		vcf="results/population-typing/{source}/evaluation/plots/vcfs/{callset}_intersection_all_full_raw.vcf.gz",
		filters= lambda wildcards: "results/population-typing/{source}/evaluation/statistics/plot_bi_all_filters.tsv" if wildcards.callset == 'hprc' else config['hgsvc_filters']
	output:
		"results/population-typing/{source}/evaluation/plots/vcfs/{callset}_intersection_{filter}_full_raw.vcf.gz"
	resources:
		mem_total_mb=80000,
		runtime_hrs=5,
		runtime_min=59
	wildcard_constraints:
		callset = 'hprc|hgsvc',
		filter = 'strict|lenient'
	shell:
		"""
		zcat {input.vcf} | python3 workflow/scripts/select_ids.py {input.filters} {wildcards.filter} | bgzip -c > {output}
		tabix -p vcf {output}
		"""

# prepare HPRC panel
rule annotate_panel_vcf:
	input:
		vcf="results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf.gz"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel.vcf.gz"
	conda:
		"../envs/whatshap.yml"
	resources:
		mem_total_mb=80000,
		runtime_hrs=5,
		runtime_min=59
	shell:
		"""
		zcat {input.vcf} | python3 workflow/scripts/extract-varianttype.py large | bcftools +fill-tags -Oz -o {output} -- -t AN,AC,AF
		tabix -p vcf {output}
		"""

# prepare HGSVC panel
rule annotate_hgsvc_panel:
	input:
		vcf=config['hgsvc_vcf']
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel-hgsvc.vcf.gz"
	conda:
		"../envs/whatshap.yml"
	shell:
		"""
		zcat {input.vcf} | python3 workflow/scripts/extract-varianttype.py large | bcftools +fill-tags -Oz -o {output} -- -t AN,AC,AF
		tabix -p vcf {output}
		"""


###################################################################################################################################################################################################
###############################  create violin plots showing the number of SVs per sample in the different callsets (for different allele frequency cutoffs)  #####################################
###################################################################################################################################################################################################

def annotate_calls_files(wildcards):
	if wildcards.annotation == 'giab':
		return ["resources/GRCh38_notinalldifficultregions.bed",
			"resources/GRCh38_alllowmapandsegdupregions.bed",
			"resources/GRCh38_allOtherDifficultregions.bed",
			"resources/GRCh38_AllTandemRepeats_gt100bp_slop5.bed"]
	else:
		assert wildcards.annotation == 'repeats'
		return ["resources/GRCh38_Alu.bed",
			"resources/GRCh38_L1.bed",
			"resources/GRCh38_SVA.bed",
			"resources/GRCh38_ERV.bed",
			"resources/GRCh38_Other-TE.bed",
			"resources/GRCh38_Satellite.bed",
			"resources/GRCh38_VNTR.bed",
			"resources/GRCh38_STR.bed",
			"resources/GRCh38_Other-LCR.bed",
			"resources/GRCh38_Other-repeat.bed",
			"resources/GRCh38_SegDup.bed",
			"resources/GRCh38_Low-repeat.bed"
			]


def annotate_calls_names(wildcards):
	if wildcards.annotation == 'giab':
		return ["Easy",
			"LowMap_SegDup",
			"OtherDifficult",
			"TandemRepeat"]
	else:
		assert wildcards.annotation == 'repeats'
		return [
			"Alu",
			"L1",
			"SVA",
			"ERV",
			"Other-TE",
			"Satellite",
			"VNTR",
			"STR",
			"Other-LCR",
			"Other-repeat",
			"SegDup",
			"Low-repeat"
		]

rule annotate_calls:
	input:
		vcf="{filename}.vcf.gz",
		beds = annotate_calls_files
	output:
		 "{filename}_annotated-{annotation}.txt.gz"
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=80000
	params:
		names= annotate_calls_names
	wildcard_constraints:
		annotation = "giab|repeats"
	shell:
		"bedtools annotate -i {input.vcf} -files {input.beds} | python3 workflow/scripts/annotate_repeats.py -vcf {input.vcf} -names {params.names} -format vcf | bgzip > {output}"


rule plot_sv_counts_all:
	input:
		hprc_panel = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel_annotated-{annotation}.txt.gz",
		hprc_all = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hprc_intersection_all_full_raw_annotated-{annotation}.txt.gz",
		populations = "resources/20130606_g1k_3202_samples_ped_population.ext.tsv"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison_{annotation}.pdf"
	log:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison_{annotation}.log"
	conda:
		"../envs/plotting.yml"
	params:
		names='HPRC-panel HPRC-PanGenie-all',
		outname="results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison_{annotation}",
		annotations= annotate_calls_names,
		param = lambda wildcards: '--only-regions' if wildcards.annotation == 'repeats' else ''
	wildcard_constraints:
		annotation = "giab|repeats"
	shell:
		"python3 workflow/scripts/plot-sv-counts.py -vcfs {input.hprc_panel} {input.hprc_all} -names {params.names} -o {params.outname} {params.param} -pop {input.populations} --annotations {params.annotations} &> {log}"


rule plot_sv_counts_filtered:
	input:
		illumina = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/illumina_intersection_all_full_raw_annotated-{annotation}.txt.gz",
		hprc = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hprc_intersection_{mode}_full_{merging}_annotated-{annotation}.txt.gz",
		hgsvc = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hgsvc_intersection_{mode}_full_raw_annotated-{annotation}.txt.gz",
		populations = "resources/20130606_g1k_3202_samples_ped_population.ext.tsv"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}_{annotation}_{merging}.pdf"
	log:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}_{annotation}_{merging}.log"
	wildcard_constraints:
		mode = "all|strict|lenient",
		annotation = "giab",
		merging = "collapsed|raw"
	conda:
		"../envs/plotting.yml"
	params:
		names = 'HPRC-PanGenie-{mode} HGSVC-PanGenie-{mode} illumina-all',
		outname = "results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}_{annotation}_{merging}",
		annotations = annotate_calls_names,
		param = lambda wildcards: '--only-regions' if wildcards.annotation == 'repeats' else '',
		hap = lambda wildcards: "--hap" if wildcards.merging == "raw" else ""
	shell:
		"python3 workflow/scripts/plot-sv-counts.py {params.hap} -vcfs {input.hprc} {input.hgsvc} {input.illumina} -names {params.names} -o {params.outname} {params.param} -pop {input.populations} --annotations {params.annotations} &> {log}"



rule plot_sv_counts_filtered_repeats:
	input:
		hprc = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hprc_intersection_{mode}_full_{merging}_annotated-{annotation}.txt.gz",
		populations = "resources/20130606_g1k_3202_samples_ped_population.ext.tsv"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}_{annotation}_{merging}.pdf"
	log:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}_{annotation}_{merging}.log"
	wildcard_constraints:
		mode = "all|strict|lenient",
		annotation = "repeats",
		merging = "collapsed|raw"
	conda:
		"../envs/plotting.yml"
	params:
		names = 'HPRC-PanGenie-{mode}',
		outname = "results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}_{annotation}_{merging}",
		annotations = annotate_calls_names,
		param = lambda wildcards: '--only-regions' if wildcards.annotation == 'repeats' else '',
		hap = lambda wildcards: "--hap" if wildcards.merging == "raw" else ""
	shell:
		"python3 workflow/scripts/plot-sv-counts.py {params.hap} -vcfs {input.hprc} -names {params.names} -o {params.outname} {params.param} -pop {input.populations} --annotations {params.annotations} &> {log}"





######################################################################################################################
############################# plot length distribution for the different callsets ####################################
######################################################################################################################


rule extract_population_samples:
	input:
		"resources/20130606_g1k_3202_samples_ped_population.ext.tsv"
	output:
		"results/population-typing/{source}/evaluation/plots/samples/samples-{population}.tsv"
	wildcard_constraints:
		population = "AFR|AMR|EAS|EUR|SAS"
	shell:
		"awk '$7==\"{wildcards.population}\"' {input} | cut -f 2 > {output}"


rule plot_length_distribution_per_pop:
	input:
		illumina = "results/population-typing/{source}/evaluation/plots/vcfs/illumina_intersection_all_full_raw.vcf.gz",
		hprc = "results/population-typing/{source}/evaluation/plots/vcfs/hprc_intersection_{mode}_full_{merging}.vcf.gz",
		hgsvc = "results/population-typing/{source}/evaluation/plots/vcfs/hgsvc_intersection_{mode}_full_raw.vcf.gz",
		samples = "results/population-typing/{source}/evaluation/plots/samples/samples-{population}.tsv"
	output:
		illumina = "results/population-typing/{source}/evaluation/plots/vcfs/illumina_{mode}_{merging}_{population}.vcf.gz",
		hprc = temp("results/population-typing/{source}/evaluation/plots/vcfs/hprc_{mode}_{merging}_{population}.vcf.gz"),
		hgsvc = temp("results/population-typing/{source}/evaluation/plots/vcfs/hgsvc_{mode}_{merging}_{population}.vcf.gz"),
		pdf = "results/population-typing/{source}/evaluation/plots/length-distribution-{mode}_{merging}_{population}.pdf"
	log:
		"results/population-typing/{source}/evaluation/plots/length-distribution-{mode}_{merging}_{population}.log"
	wildcard_constraints:
		mode = "all|strict|lenient",
		population = "AFR|AMR|EAS|EUR|SAS",
		merging = "collapsed|raw"
	conda:
		"../envs/genotyping.yml"
	params:
		names = 'HPRC-PanGenie-{mode}-{population} HGSVC-PanGenie-{mode}-{population} illumina-all-{population}'
	shell:
		"""
		bcftools view --samples-file {input.samples} --force-samples {input.hprc} | bcftools +fill-tags -Oz -o {output.hprc} -- -t AN,AC,AF
		bcftools view --samples-file {input.samples} --force-samples {input.hgsvc} | bcftools +fill-tags -Oz -o {output.hgsvc} -- -t AN,AC,AF
		bcftools view --samples-file {input.samples} --force-samples {input.illumina} | bcftools +fill-tags -Oz -o {output.illumina} -- -t AN,AC,AF

		python3 workflow/scripts/plot-variant-length.py --callsets {output.hprc} {output.hgsvc} {output.illumina} --names {params.names} -a 0.05 -o {output.pdf} &> {log}
		"""


rule plot_length_distribution:
	input:
		illumina = "results/population-typing/{source}/evaluation/plots/vcfs/illumina_intersection_all_full_raw.vcf.gz",
		hprc = "results/population-typing/{source}/evaluation/plots/vcfs/hprc_intersection_{mode}_full_{merging}.vcf.gz",
		hgsvc = "results/population-typing/{source}/evaluation/plots/vcfs/hgsvc_intersection_{mode}_full_raw.vcf.gz"
	output:
		"results/population-typing/{source}/evaluation/plots/length-distribution-{mode}_{merging}.pdf"
	log:
		"results/population-typing/{source}/evaluation/plots/length-distribution-{mode}_{merging}.log"
	wildcard_constraints:
		mode = "all|strict|lenient",
		merging = "collapsed|raw"
	conda:
		"../envs/plotting.yml"
	params:
		names = 'HPRC-PanGenie-{mode} HGSVC-PanGenie-{mode} illumina-all',
	shell:
		"python3 workflow/scripts/plot-variant-length.py --callsets {input.hprc} {input.hgsvc} {input.illumina} --names {params.names} -a 0.05 -o {output} &> {log}"

