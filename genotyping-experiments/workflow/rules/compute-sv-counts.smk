configfile: "config/config.yaml"

callset_to_vcf = {
	"hgsvc": config['hgsvc_genotypes'],
	"hprc": "results/population-typing/{source}/merged-vcfs/whole-genome/all-samples_bi_all.vcf.gz",
	"illumina": config['illumina']
}

sample_to_panel = {
	"full" : "",
	"HG01083" : "",
	"NA12878" : "in_HGSVC-panel",
	"HG00438" : "in_HPRC-panel"
}

callset_panel_samples = {
	"hprc" : [s for s in config['assembly_samples']],
	"hgsvc": [s for s in config['hgsvc_samples']]
}


## Note: this pipeline considers only chromosomes 1-22 + chr X

# also determine the list of samples contained in each VCF
rule prepare_vcf:
	input:
		lambda wildcards: callset_to_vcf[wildcards.callset]
	output:
		samples = "results/population-typing/{source}/evaluation/plots/vcfs/{callset}_all.tsv"
	wildcard_constraints:
		callset = 'hprc|hgsvc|illumina'
	log:
		"results/population-typing/{source}/evaluation/plots/vcfs/{callset}_all.log"
	conda:
		"../envs/whatshap.yml"
	shell:
		"""
		bcftools query -l {input} > {output.samples}
		"""

# find out which samples are contained in HPRC/HGSVC vcfs
rule intersect_samples:
	input:
		samples = expand("results/population-typing/{{source}}/evaluation/plots/vcfs/{callset}_all.tsv", callset=['hgsvc', 'hprc', 'illumina'])
	output:
		"results/population-typing/{source}/evaluation/plots/vcfs/samples-intersection.tsv"
	run:
		result = None
		for sample in input.samples:
			names = set([s.strip() for s in open(sample, 'r')])
			if result is None:
				result = names
			else:
				result = result & names
		with open(output[0], 'w') as outfile:
			for s in result:
				outfile.write(s + '\n')


# keep only intersection of samples (so that VCFs are comparable)
# add AF,AN,AC tags to VCFs (to make sure all VCFs have them)
rule extract_intersection_samples:
	input:
		vcf= lambda wildcards: callset_to_vcf[wildcards.callset],
		samples = "results/population-typing/{source}/evaluation/plots/vcfs/samples-intersection.tsv"
	output:
		"results/population-typing/{source}/evaluation/plots/vcfs/{callset}_intersection_all_full.vcf.gz"
	conda:
		"../envs/whatshap.yml"
	resources:
		mem_total_mb=80000,
		runtime_hrs=23,
		runtime_min=59
	shell:
		"""
		bcftools view --samples-file {input.samples} {input.vcf} | python3 workflow/scripts/extract-varianttype.py large | bcftools +fill-tags -Oz -o {output} -- -t AN,AC,AF
		tabix -p vcf {output}
		"""


# select high conf variants from HGSVC HPRC after intersection of variants
rule produce_filtered_callsets:
	input:
		vcf="results/population-typing/{source}/evaluation/plots/vcfs/{callset}_intersection_all_full.vcf.gz",
		filters= lambda wildcards: "results/population-typing/{source}/evaluation/statistics/plot_bi_all_filters.tsv" if wildcards.callset == 'hprc' else config['hgsvc_filters']
	output:
		"results/population-typing/{source}/evaluation/plots/vcfs/{callset}_intersection_{filter}_full.vcf.gz"
	resources:
		mem_total_mb=20000,
		runtime_hrs=2,
		runtime_min=59
	wildcard_constraints:
		callset = 'hgsvc|hprc',
		filter = 'strict|lenient'
	shell:
		"zcat {input.vcf} | python3 workflow/scripts/select_ids.py {input.filters} {wildcards.filter} | bgzip -c > {output}"


# prepare panel to be plotted
rule annotate_panel_vcf:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf.gz"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel.vcf.gz"
	conda:
		"../envs/whatshap.yml"
	shell:
		"zcat {input} | python3 workflow/scripts/extract-varianttype.py large | bcftools +fill-tags -Oz -o {output} -- -t AN,AC,AF"

rule annotate_hgsvc_panel:
	input:
		config['hgsvc_vcf']
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel-hgsvc.vcf.gz"
	conda:
		"../envs/whatshap.yml"
	shell:
		"zcat {input} | python3 workflow/scripts/extract-varianttype.py large | bcftools +fill-tags -Oz -o {output} -- -t AN,AC,AF"




###################################################################################################################################################################################################
###############################  create violin plots showing the number of SVs per sample in the different callsets (for different allele frequency cutoffs)  #####################################
###################################################################################################################################################################################################

rule annotate_calls:
	input:
		vcf="{filename}.vcf.gz",
		bed1="resources/GRCh38_notinalldifficultregions.bed",
		bed2="resources/GRCh38_alllowmapandsegdupregions.bed",
		bed3="resources/GRCh38_allOtherDifficultregions.bed",
		bed4="resources/GRCh38_AllTandemRepeats_gt100bp_slop5.bed"
	output:
		 "{filename}_annotated.txt.gz"
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=80000
	params:
		names="Easy LowMap_SegDup OtherDifficult TandemRepeat"
	shell:
		"bedtools annotate -i {input.vcf} -files {input.bed1} {input.bed2} {input.bed3} {input.bed4} | python3 workflow/scripts/annotate_repeats.py -vcf {input.vcf} -names {params.names} -format vcf | bgzip > {output}"


rule plot_sv_counts_all:
	input:
		hprc_panel = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel_annotated.txt.gz",
		hprc_all = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hprc_intersection_all_full_annotated.txt.gz",
		populations = "resources/20130606_g1k_3202_samples_ped_population.ext.tsv"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison.pdf"
	log:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison.log"
	conda:
		"../envs/plotting.yml"
	params:
		names='HPRC-panel HPRC-PanGenie-all',
		outname="results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison",
		annotations="Easy LowMap_SegDup OtherDifficult TandemRepeat"
	shell:
		"python3 workflow/scripts/plot-sv-counts.py -vcfs {input.hprc_panel} {input.hprc_all} -names {params.names} -o {params.outname} -pop {input.populations} --annotations {params.annotations} &> {log}"


rule plot_sv_counts_filtered:
	input:
		illumina = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/illumina_intersection_all_full_annotated.txt.gz",
		hprc = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hprc_intersection_{mode}_full_annotated.txt.gz",
		hgsvc = "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hgsvc_intersection_{mode}_full_annotated.txt.gz",
		populations = "resources/20130606_g1k_3202_samples_ped_population.ext.tsv"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}.pdf"
	log:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}.log"
	wildcard_constraints:
		mode = "all|strict|lenient"
	conda:
		"../envs/plotting.yml"
	params:
		names = 'HPRC-PanGenie-{mode} HGSVC-PanGenie-{mode} illumina-all',
		outname = "results/population-typing/{caller}-{threshold}/evaluation/plots/sv-count-comparison-{mode}",
		annotations = "Easy LowMap_SegDup OtherDifficult TandemRepeat"
	shell:
		"python3 workflow/scripts/plot-sv-counts.py -vcfs {input.hprc} {input.hgsvc} {input.illumina} -names {params.names} -o {params.outname} -pop {input.populations} --annotations {params.annotations} &> {log}"


######################################################################################################################
############################# plot length distribution for the different callsets ####################################
######################################################################################################################

rule plot_length_distribution:
	input:
		illumina = "results/population-typing/{source}/evaluation/plots/vcfs/illumina_intersection_all_full.vcf.gz",
		hprc = "results/population-typing/{source}/evaluation/plots/vcfs/hprc_intersection_{mode}_full.vcf.gz",
		hgsvc = "results/population-typing/{source}/evaluation/plots/vcfs/hgsvc_intersection_{mode}_full.vcf.gz"
	output:
		"results/population-typing/{source}/evaluation/plots/length-distribution-{mode}.pdf"
	log:
		"results/population-typing/{source}/evaluation/plots/length-distribution-{mode}.log"
	wildcard_constraints:
		mode = "all|strict|lenient"
	conda:
		"../envs/plotting.yml"
	params:
		names = 'HPRC-PanGenie-{mode} HGSVC-PanGenie-{mode} illumina-all',
	shell:
		"python3 workflow/scripts/plot-variant-length.py --callsets {input.hprc} {input.hgsvc} {input.illumina} --names {params.names} -a 0.05 -o {output} &> {log}"



######################################################################################################################
############### intersect callsets based on overlaps/breakpoint deviations and create upset plots ####################
######################################################################################################################


# extract a specific sample from the VCFs (to do a sample-specific plot)
rule extract_genotyped_sample:
	input:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/{callset}_intersection_{mode}_full.vcf.gz"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/{callset}_intersection_{mode}_{sample}.vcf.gz"
	log:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/{callset}_intersection_{mode}_{sample}.log"
	conda:
		"../envs/whatshap.yml"
	wildcard_constraints:
		mode = "all|strict|lenient"
	params:
		samples = lambda wildcards: ','.join(callset_panel_samples[wildcards.sample]) if wildcards.sample in ['hgsvc', 'hprc'] else wildcards.sample,
		force_samples = lambda wildcards: "--force-samples" if wildcards.sample in ['hgsvc', 'hprc'] else ""
	shell:
		"""
		zcat {input} | bcftools view --samples {params.samples} {params.force_samples} | bcftools view --min-ac 1 | bgzip > {output}
		"""

rule extract_panel_sample:
	input:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/{panel}.vcf.gz"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/{panel}_{sample}.vcf.gz"
	conda:
		"../envs/whatshap.yml"
	shell:
		"""
		zcat {input} | bcftools view --samples {wildcards.sample} | bcftools view --min-ac 1 | bgzip > {output}
		"""


def intersect_callsets_input(wildcards):
	files = []
	panel = sample_to_panel[wildcards.samples]
	if panel == "in_HPRC-panel":
		files.append("results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel_{samples}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, samples=wildcards.samples))
	files.append("results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hprc_intersection_{mode}_{samples}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, mode=wildcards.mode, samples=wildcards.samples))
	if panel == "in_HGSVC-panel":
		files.append("results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel-hgsvc_{samples}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, samples=wildcards.samples))
	files.append("results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/hgsvc_intersection_{mode}_{samples}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, mode=wildcards.mode, samples=wildcards.samples))
	files.append("results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/illumina_intersection_all_{samples}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, mode=wildcards.mode, samples=wildcards.samples))
	return files

def intersect_callsets_names(wildcards):
	names = []
	panel = sample_to_panel[wildcards.samples]
	if panel == "in_HPRC-panel":
		names.append("HPRC-panel")
	names.append("HPRC-PanGenie-{mode}".format(mode=wildcards.mode))
	if panel == "in_HGSVC-panel":
		names.append("HGSVC-panel")
	names.append("HGSVC-PanGenie-{mode}".format(mode=wildcards.mode))
	names.append("illumina-all")
	return names


rule intersect_callsets:
	input:
		files = intersect_callsets_input
	output:
		tsv="results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}.tsv",
		vcf="results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}.vcf"
	conda:
		"../envs/plotting.yml"
	wildcard_constraints:
		mode = "all|strict|lenient"
	resources:
		mem_total_mb=20000,
		runtime_hrs=2,
		runtime_min=1
	log:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}.log"
	params:
		names = intersect_callsets_names,
	shell:
		"python3 workflow/scripts/intersect-callsets.py intersect -t {output.tsv} -v {output.vcf} -c {input.files} -n {params.names} &> {log}"


rule annotate_intersected_calls:
	input:
		vcf="results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}.vcf",
		bed1="resources/GRCh38_notinalldifficultregions.bed",
		bed2="resources/GRCh38_alllowmapandsegdupregions.bed",
		bed3="resources/GRCh38_allOtherDifficultregions.bed",
		bed4="resources/GRCh38_AllTandemRepeats_gt100bp_slop5.bed"
	output:
		temp("results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}_annotations.tsv")
	conda:
		"../envs/genotyping.yml"
	resources:
		mem_total_mb=30000
	params:
		names="Easy LowMap_SegDup OtherDifficult TandemRepeat"
	shell:
		"bedtools annotate -i {input.vcf} -files {input.bed1} {input.bed2} {input.bed3} {input.bed4} | python3 workflow/scripts/annotate_repeats.py -names {params.names} -format tsv > {output}"


rule add_annotations:
	input:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}.tsv",
		"results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}_annotations.tsv"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}_annotated.tsv"
	conda:
		"../envs/plotting.yml"
	resources:
		mem_total_mb=30000
	shell:
		"python3 workflow/scripts/merge-tables.py {input} {output}" 


rule plot_upset:
	input:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/callset-intersection/intersection-{mode}_{samples}_annotated.tsv"
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/intersection-upset-{mode}_{samples}.pdf"
	conda:
		"../envs/plotting.yml"
	wildcard_constraints:
		mode = "all|strict|lenient"
	params:
		names= lambda wildcards: 'in_HPRC-PanGenie-{mode} in_HGSVC-PanGenie-{mode} in_illumina-all '.format(mode=wildcards.mode) +  sample_to_panel[wildcards.samples],
		regions = "Easy LowMap_SegDup OtherDifficult TandemRepeat"
	shell:
		"python3 workflow/scripts/plot_upset.py --table {input} --output {output} --names {params.names} --regions {params.regions}"



#################################################################################################################################
################################ compute distance to closest variant in other callsets ##########################################
#################################################################################################################################


def compute_closest_input(wildcards):
	result = []
	for callset in [wildcards.callset1, wildcards.callset2]:
		if callset in ["hprc", "hgsvc"]:
			result.append("results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/{callset}_intersection_{mode}_{sample}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, callset=callset, mode=wildcards.mode, sample=wildcards.sample))
		else:
			assert callset == "illumina"
			result.append("results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/illumina_intersection_all_{sample}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, sample=wildcards.sample))
	return result


rule compute_closest:
	input:
		compute_closest_input
	output:
		distances="results/population-typing/{caller}-{threshold}/evaluation/plots/distances/distances_{callset1}_{callset2}_{mode}_{sample}.txt",
	params:
		names = "{callset1}-{callset2}_{mode}_{sample}"
	resources:
		mem_total_mb=50000
	conda:
		"../envs/plotting.yml"
	shell:
		"""
		bedtools closest -d -t first -a {input[0]} -b {input[1]} > {output.distances}
		"""

rule plot_distances:
	input:
		expand("results/population-typing/{{caller}}-{{threshold}}/evaluation/plots/distances/distances_{callset1}_{callset2}_{{mode}}_{{sample}}.txt", callset1=["hprc", "hgsvc", "illumina"], callset2=["hprc", "hgsvc", "illumina"])
	output:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/distances_{mode}_{sample}.pdf"
	resources:
		mem_total_mb=50000
	conda:
		"../envs/plotting.yml"
	log:
		"results/population-typing/{caller}-{threshold}/evaluation/plots/distances_{mode}_{sample}.log"
	shell:
		"python3 workflow/scripts/plot-distances.py {input} {output} > {log}"


########### for HPRC variants with large distance to HGSVC variants, check whether these calls are in HGSVC PAV panel ###########

rule check_distant_variants_hprc:
	input:
		vcf=config['hgsvc_vcf'], #if wildcards.sample == "hgsvc" else "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel-hgsvc_{sample}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, sample=wildcards.sample),
		distances="results/population-typing/{caller}-{threshold}/evaluation/plots/distances/distances_hprc_hgsvc_{mode}_{sample}.txt"
	output:
		distant="results/population-typing/{caller}-{threshold}/evaluation/plots/distances/analysis-distant-variants/unique_hprc_hgsvc_{mode}_{sample}.vcf",
		txt="results/population-typing/{caller}-{threshold}/evaluation/plots/distances/analysis-distant-variants/distances-unique_hprc_hgsvc_{mode}_{sample}.txt"
	conda:
		"../envs/plotting.yml"
	params:
		distance_threshold = 1000
	wildcard_constraints:
		sample="hgsvc|NA12878"
	shell:
		"""
		cat {input.distances} | python3 workflow/scripts/extract-distant-vars.py {params.distance_threshold} > {output.distant}
		bedtools closest -d -t first -a {output.distant} -b {input.vcf} > {output.txt}
		"""

########### for HGSVC variants with large distance to HPRC variants, check whether these calls are in HPRC  panel ###########
# NOTE: this is difficult, because we don't have decomposed panel before filtering. What is used here is the HPRC panel after the filtering steps.

rule check_distant_variants_hgsvc:
	input:
		vcf= "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel.vcf.gz", #if wildcards.sample == "hprc" else "results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/panel_{sample}.vcf.gz".format(caller=wildcards.caller, threshold=wildcards.threshold, sample=wildcards.sample),
		distances="results/population-typing/{caller}-{threshold}/evaluation/plots/distances/distances_hgsvc_hprc_{mode}_{sample}.txt"
	output:
		vcf=temp("results/population-typing/{caller}-{threshold}/evaluation/plots/vcfs/tmp-sorted-panel.vcf.gz"),
		distant="results/population-typing/{caller}-{threshold}/evaluation/plots/distances/analysis-distant-variants/unique_hgsvc_hprc_{mode}_{sample}.vcf",
		txt="results/population-typing/{caller}-{threshold}/evaluation/plots/distances/analysis-distant-variants/distances-unique_hgsvc_hprc_{mode}_{sample}.txt"
	conda:
		"../envs/plotting.yml"
	params:
		distance_threshold = 1000
	wildcard_constraints:
		sample="hgsvc|NA12878"
	shell:
		"""
		cat {input.distances} | python3 workflow/scripts/extract-distant-vars.py {params.distance_threshold} > {output.distant}
		cat {input.vcf} | awk '$1 ~ /^#/ {{print $0;next}} {{print $0 | \" sort -k1,1 -k2,2n \" }} | bgzip > {output.vcf}
		tabix -p vcf {output.vcf}
		bedtools closest -d -t first -a {output.distant} -b {output.vcf} > {output.txt}
		"""
