# get reference sequence (hg38)
rule download_reference_hg38:
	output:
		fasta="results/data/fasta/hg38.fa"
	shell:
		"wget -O {output} https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz"

# index hg38 fasta
rule index:
	input:
		"results/data/fasta/hg38.fa"
	output:
		"results/data/fasta/hg38.fa.fai"
	conda:
		"../envs/genotyping.yml"
	shell:
		"samtools faidx {input}"

