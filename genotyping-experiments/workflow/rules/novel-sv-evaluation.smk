configfile: "config/config.yaml"


# find out which variants are novel. These are defined as being in HPRC (1/. in the 5 evaluation samples), made it into the lenient set and are not in the Illumina set.
# in order to decide whether they are not in the illumina set: distance to next variant > X, no overlap

leave_one_out_samples = ['HG00438', 'HG00733', 'HG02717', 'NA20129', 'HG03453']
variants = ['large-deletion', 'large-insertion', 'large-complex']


##########################################################################
# find novel SV variants in lenient set (and compute the inverse)
##########################################################################


# biallelic panel SVs
rule extract_panel_svs:
	input:
		"results/data/vcf/{caller}-{threshold}/{caller}_filtered_ids_biallelic.vcf.gz"
	output:
		vcf = temp("results/novel-sv-evaluation/{caller}-{threshold}/panel/panel-svs.vcf.gz"),
		tbi = temp("results/novel-sv-evaluation/{caller}-{threshold}/panel/panel-svs.vcf.gz.tbi")
	shell:
		"""
		zcat {input} | python3 workflow/scripts/extract-varianttype.py sv | bgzip -c > {output.vcf}
		tabix -p vcf {output.vcf}
		"""


# find variants with no reciprocal overlap in Illiumina callset
rule svs_not_in_illumina:
	input:
		panel_vcf="results/novel-sv-evaluation/{source}/panel/panel-svs.vcf.gz",
		panel_tbi="results/novel-sv-evaluation/{source}/panel/panel-svs.vcf.gz.tbi",
		illumina_vcf = config["illumina"]
	output:
		ids = "results/novel-sv-evaluation/{source}/known-ids/illumina-novel.tsv",
		tsv = "results/novel-sv-evaluation/{source}/known-ids/intersection-illumina.tsv",
		vcf = temp("results/novel-sv-evaluation/{source}/known-ids/intersection-panel-illumina.vcf")
	conda:
		"../envs/plotting.yml"
	resources:
		mem_total_mb=20000,
		runtime_hrs=2,
		runtime_min=1
	log:
		"results/novel-sv-evaluation/{source}/known-ids/intersection-panel-illumina.log"
	shell:
		"""
		python3 workflow/scripts/intersect-callsets.py intersect -t {output.tsv} -v {output.vcf} -c {input.panel_vcf} {input.illumina_vcf} -n panel illumina &> {log}
		awk '($9 == \"True\") && ($10 == \"False\")' {output.tsv} | cut -f 1 > {output.ids}
		"""


# extract list of lenient SV ids
rule lenient_svs:
	input:
		"results/population-typing/{source}/evaluation/statistics/plot_bi_all_filters.tsv"
	output:
		"results/novel-sv-evaluation/{source}/known-ids/lenient.tsv"
	shell:
		"""
		awk '($8 >= 1) && ( ($16==\"True\") || ($17==\"True\") || ($18 == \"True\"))' {input} | cut -f 1 > {output}
		"""		


# define novel/known svs part of the lenient set and output the complements of these sets
rule find_novel_svs:
	input:
		panel_vcf = "results/novel-sv-evaluation/{source}/panel/panel-svs.vcf.gz",
		panel_tbi = "results/novel-sv-evaluation/{source}/panel/panel-svs.vcf.gz.tbi",
		lenient = "results/novel-sv-evaluation/{source}/known-ids/lenient.tsv",
		illumina = "results/novel-sv-evaluation/{source}/known-ids/illumina-novel.tsv",
		untypables = "results/data/vcf/{source}/untypable-ids/{sample}-untypable.tsv"
	output:
		# files contain the complement of variants (so that removing them from set results in the desired set of variants)
		novel = "results/novel-sv-evaluation/{source}/known-ids/{sample}-novellenient.tsv",
		known = "results/novel-sv-evaluation/{source}/known-ids/{sample}-knownlenient.tsv",
		lenient = "results/novel-sv-evaluation/{source}/known-ids/{sample}-lenient.tsv",
		novel_tmp = temp("results/novel-sv-evaluation/{source}/known-ids/tmp_{sample}-novellenient.tsv"),
		known_tmp = temp("results/novel-sv-evaluation/{source}/known-ids/tmp_{sample}-knownlenient.tsv"),
		lenient_tmp = temp("results/novel-sv-evaluation/{source}/known-ids/tmp_{sample}-lenient.tsv") 

	log:
		"results/novel-sv-evaluation/{source}/known-ids/{sample}-known.log"
	params:
		samples = ' '.join(leave_one_out_samples),
		outname = "results/novel-sv-evaluation/{source}/known-ids/tmp_{sample}"
	shell:
		"""
		zcat {input.panel_vcf} | python3 workflow/scripts/find-novel-svs.py {input.lenient} {input.illumina} -samples {params.samples} -outname {params.outname} &> {log}
		cat {output.novel_tmp} {input.untypables} | sort | uniq > {output.novel}
		cat {output.known_tmp} {input.untypables} | sort | uniq > {output.known}
		cat {output.lenient_tmp} {input.untypables} | sort | uniq > {output.lenient}
		"""



###################################################################################
#  remove these variants from the genotyped sets. Keep untypables and extract SVs
###################################################################################


rule filter_genotypes:
	input:
		vcf="results/leave-one-out/{source}/{sample}{other}.vcf",
		ids="results/novel-sv-evaluation/{source}/known-ids/{sample}-{strat}.tsv"
	output:
		vcf="results/novel-sv-evaluation/{source}/{sample}{other}-novel-{vartype}-{strat}.vcf.gz",
		tbi="results/novel-sv-evaluation/{source}/{sample}{other}-novel-{vartype}-{strat}.vcf.gz.tbi"
	wildcard_constraints:
		sample="|".join(leave_one_out_samples),
		vartype="|".join(variants),
		strat = "novellenient|knownlenient|lenient"
	resources:
		mem_total_mb=20000,
		runtime_hrs=1
	shell:
		"""
		cat {input.vcf} | python3 workflow/scripts/skip-untypable.py {input.ids} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} | bgzip -c > {output.vcf}
		tabix -p vcf {output.vcf}
		"""





###################################################################
# compute genotype concordance
###################################################################

def region_to_bed(wildcards):
	if wildcards.regions == "easy":
		return "resources/GRCh38_notinalldifficultregions.bed"
	if wildcards.regions == "lowmap":
		return "resources/GRCh38_alllowmapandsegdupregions.bed"
	if wildcards.regions == "repeat":
		return "resources/GRCh38_AllTandemRepeats_gt100bp_slop5.bed"
	if wildcards.regions == "other":
		return "resources/GRCh38_allOtherDifficultregions.bed"
	if wildcards.regions == "biallelic":
		return "results/data/vcf/{source}/biallelic-bubbles.bed".format(source=wildcards.source)
	if wildcards.regions == "multiallelic":
		return "results/data/vcf/{source}/complex-bubbles.bed".format(source=wildcards.source)
	assert(False)



rule collect_typed_novel_variants:
	input:
		callset="results/leave-one-out/{source}/{sample}/preprocessed-vcfs/{source}_filtered_ids_biallelic_no-missing.vcf.gz",
		ids="results/novel-sv-evaluation/{source}/known-ids/{sample}-{strat}.tsv",
		regions=region_to_bed
	output:
		"results/novel-sv-evaluation/{source}/{sample}/genotyped-ids/{regions}-{strat}_{vartype}.tsv"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		sample = "|".join(leave_one_out_samples),
		vartype = "|".join(variants),
		regions = "easy|lowmap|repeat|other|biallelic|multiallelic",
		strat = "novellenient|knownlenient|lenient"
	resources:
		mem_total_mb=50000
	priority: 1
	shell:
		"zcat {input.callset} | python3 workflow/scripts/skip-untypable.py {input.ids} | python3 workflow/scripts/extract-varianttype.py {wildcards.vartype} | bedtools intersect -header -a - -b {input.regions} -u -f 0.5 | python3 workflow/scripts/get_ids.py > {output}"



rule evaluate_novel:
	input:
		callset="results/novel-sv-evaluation/{source}/{sample}/pangenie/pangenie-{sample}_genotyping-biallelic-novel-{vartype}-{strat}.vcf.gz",
		callset_tbi="results/novel-sv-evaluation/{source}/{sample}/pangenie/pangenie-{sample}_genotyping-biallelic-novel-{vartype}-{strat}.vcf.gz.tbi",
		baseline="results/novel-sv-evaluation/{source}/{sample}/truth/truth-{sample}-novel-{vartype}-{strat}.vcf.gz",
		baseline_tbi="results/novel-sv-evaluation/{source}/{sample}/truth/truth-{sample}-novel-{vartype}-{strat}.vcf.gz.tbi",
		typed_ids = "results/novel-sv-evaluation/{source}/{sample}/genotyped-ids/{regions}-{strat}_{vartype}.tsv",
		regions= region_to_bed
	output:
		tmp_vcf1=temp("results/novel-sv-evaluation/{source}/{sample}/evaluation/concordance/pangenie/{regions}-{strat}_{vartype}/baseline.vcf"),
		tmp_vcf2=temp("results/novel-sv-evaluation/{source}/{sample}/evaluation/concordance/pangenie/{regions}-{strat}_{vartype}/callset.vcf"),
		summary="results/novel-sv-evaluation/{source}/{sample}/evaluation/concordance/pangenie/{regions}-{strat}_{vartype}/summary.txt"
	conda:
		"../envs/genotyping.yml"
	wildcard_constraints:
		sample = "|".join(samples),
		strat = "novellenient|knownlenient|lenient"
	log:
		"results/novel-sv-evaluation/{source}/{sample}/evaluation/concordance/pangenie/{regions}-{strat}_{vartype}/summary.log"
	resources:
		mem_total_mb=40000,
		runtime_hrs=0,
		runtime_min=40
	priority: 1
	shell:
		"""
		bedtools intersect -header -a {input.baseline} -b {input.regions} -u -f 0.5 | bgzip > {output.tmp_vcf1}
		bedtools intersect -header -a {input.callset} -b {input.regions} -u -f 0.5 | bgzip > {output.tmp_vcf2}
		python3 workflow/scripts/genotype-evaluation.py {output.tmp_vcf1} {output.tmp_vcf2} {input.typed_ids} --qual 0 2> {log} 1> {output.summary}
		"""



##################################################################
# plot leave-one-out results
##################################################################


# collect results across all samples
rule collect_results_novel:
	input:
		expand("results/novel-sv-evaluation/{{source}}/{sample}/evaluation/{{metric}}/pangenie/{{regions}}-{{strat}}_{{vartype}}/summary.txt", sample = leave_one_out_samples)
	output:
		"results/novel-sv-evaluation/{source}/plots/{metric}/pangenie/{metric}_{regions}-{strat}_{vartype}.tsv"
	params:
		samples = ','.join(leave_one_out_samples),
		outfile = 'results/novel-sv-evaluation/{source}/plots/{metric}/pangenie/{metric}'
	priority: 1
	shell:
		"python3 workflow/scripts/collect-results.py {wildcards.metric} {wildcards.source} {params.samples} {wildcards.regions}-{wildcards.strat} -variants {wildcards.vartype} -folder results/novel-sv-evaluation/ -outfile {params.outfile}"



rule plotting_novel_graph:
	input:
		expand("results/novel-sv-evaluation/{{source}}/plots/{{metric}}/pangenie/{{metric}}_{regions}-{strat}_{vartype}.tsv", regions=["biallelic", "multiallelic"], strat = ["novellenient","knownlenient","lenient"], vartype=variants)
	output:
		"results/novel-sv-evaluation/{source}/plots/{metric}/pangenie/{metric}_novel_graph.pdf"
	wildcard_constraints:
		metric="concordance|precision-recall-typable",
	priority: 1
	conda:
		"../envs/genotyping.yml"
	params:
		sources = "biallelic-novellenient biallelic-knownlenient biallelic-lenient multiallelic-novellenient multiallelic-knownlenient multiallelic-lenient"
	shell:
		"python3 workflow/scripts/plot-results.py -files {input} -outname {output} -sources {params.sources} -metric {wildcards.metric}"
