import sys

import matplotlib.pyplot as plt
import numpy as np
import argparse

def plot_precision_recall(files, outname):
	var_to_name = {
		'simple_snp':'SNPs (biallelic)',
		'complex_snp':'SNPs (multiallelic)',
		'simple_small-deletion': 'deletions (biallelic, 1-19bp)',
		'simple_small-insertion': 'insertions (biallelic, 1-19bp)',
		'complex_small':'complex (1-19bp)',
		'simple_midsize-deletion':'deletions (biallelic, 20-49bp)',
		'simple_midsize-insertion':'insertions (biallelic, 20-49bp)',
		'complex_midsize':'complex (20-49bp)',
		'simple_large-deletion':'deletions (biallelic, >=50bp)',
		'simple_large-insertion':'insertions (biallelic, >=50bp)',
		'complex_large':'complex (>=50bp)'
		}
	type_to_file = {}
	n_rows = 4
	n_cols = 3
	plt.figure(figsize=(19,20))
	for f in files:
		vartype = f.replace('repeats', 'nonrep').split('nonrep-')[-1][:-4]
		type_to_file[vartype] = f
	variants = ['simple_small-deletion', 'simple_small-insertion', 'complex_small', 'simple_midsize-deletion', 'simple_midsize-insertion', 'complex_midsize', 'simple_large-deletion', 'simple_large-insertion', 'complex_large', 'simple_snp', 'complex_snp' ]
	plot_index = 1
	for var in variants:
		if var in type_to_file:
			plt.subplot(n_rows, n_cols, plot_index)
			plot_index += 1
			samples = []
			precision = []
			recall = []
			fscore = []
			for line in open(type_to_file[var], 'r'):
				if line.startswith('sample'):
					continue
				fields = line.split()
				samples.append(fields[0])
				precision.append(float(fields[1]))
				recall.append(float(fields[2]))
				fscore.append(float(fields[3]))
			x_values = [i*5 for i in range(len(samples))]
			plt.title(var_to_name[var])
			plt.plot(x_values, precision, label='precision', marker='x', linestyle='--', color='red')
			plt.plot(x_values, recall, label='recall', marker='x', color='blue', linestyle='--')
			plt.plot(x_values, fscore, label='F-score', marker='o', color='dimgray')
			plt.xticks(x_values, samples, rotation='vertical')
#			plt.gca().set_yticklabels(['{:.0f}%'.format(x) for x in plt.gca().get_yticks()])
			plt.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.3)
			plt.tight_layout()
	plt.legend()
	plt.savefig(outname)


def plot_concordances(files, outname):
	var_to_name = {
		'simple_snp':'SNPs (biallelic)',
		'complex_snp':'SNPs (multiallelic)',
		'simple_small-deletion': 'deletions (biallelic, 1-19bp)',
		'simple_small-insertion': 'insertions (biallelic, 1-19bp)',
		'complex_small':'complex (1-19bp)',
		'simple_midsize-deletion':'deletions (biallelic, 20-49bp)',
		'simple_midsize-insertion':'insertions (biallelic, 20-49bp)',
		'complex_midsize':'complex (20-49bp)',
		'simple_large-deletion':'deletions (biallelic, >=50bp)',
		'simple_large-insertion':'insertions (biallelic, >=50bp)',
		'complex_large':'complex (>=50bp)'
		}
	type_to_file = {}
	n_rows = 4
	n_cols = 3
	plt.figure(figsize=(19,20))
	for f in files:
		vartype = f.replace('repeats', 'nonrep').split('nonrep-')[-1][:-4]
		type_to_file[vartype] = f
	print(type_to_file)
	variants = ['simple_small-deletion', 'simple_small-insertion', 'complex_small', 'simple_midsize-deletion', 'simple_midsize-insertion', 'complex_midsize', 'simple_large-deletion', 'simple_large-insertion', 'complex_large', 'simple_snp', 'complex_snp' ]
	plot_index = 1
	for var in variants:
		if var in type_to_file:
			plt.subplot(n_rows, n_cols, plot_index)
			plot_index += 1
			samples = []
			concordances = []
			for line in open(type_to_file[var], 'r'):
				if line.startswith('sample'):
					continue
				fields = line.split()
				samples.append(fields[0])
				concordances.append(float(fields[1]))
			x_values = [i*5 for i in range(len(samples))]
			plt.title(var_to_name[var])
			plt.plot(x_values, concordances, label='weighted genotype concordance [%]', marker='o', color='dimgray')
			plt.xticks(x_values, samples, rotation='vertical')
#			plt.gca().set_yticklabels(['{:.0f}%'.format(x) for x in plt.gca().get_yticks()])
			plt.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.3)
			plt.tight_layout()
	plt.savefig(outname)
		

parser = argparse.ArgumentParser(prog='plot-results.py', description="Plot concordances or precision/recall statistics.")
parser.add_argument('metric', metavar='METRIC', help='evaluation metric (concordance|precision-recall-typable)')
parser.add_argument('-files', metavar='FILES', nargs='+', help='files with results per sample.')
parser.add_argument('-outname', metavar='OUTNAME', required=True, help='Name of the output file.')
args = parser.parse_args()

if args.metric == 'concordance':
	plot_concordances(args.files, args.outname)
elif args.metric == 'precision-recall-typable':
	plot_precision_recall(args.files, args.outname)
