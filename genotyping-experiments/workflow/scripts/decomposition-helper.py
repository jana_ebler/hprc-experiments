import sys
import argparse
from collections import defaultdict

def run_preprocessing():
	for line in sys.stdin:
		if line.startswith('##'):
			print(line.strip())
			continue
		fields = line.split()
		if line.startswith('#'):
			print('\t'.join(fields))
			continue
		if 'LV=0' in fields[7]:
			print('\t'.join(fields))

def run_filter(traversal, snarls):
	variants = {}
	for line in open(traversal, 'r'):
		if line.startswith('#'):
			continue
		fields = line.split()
		assert (fields[0], fields[1]) not in variants
		variants[(fields[0], fields[1])] = fields[3]

	for line in open(snarls, 'r'):
		if line.startswith('#'):
			continue
		fields = line.split()
		if (fields[0], fields[1]) in variants:
			if variants[(fields[0], fields[1])] == fields[3]:
				print(line.strip())

def determine_type(id):
	fields = id.split('-')
	if id == '':
		sys.stderr.write("Missing ID.\n")
		return None
	vartype = fields[2]
	length = int(fields[-1]) if vartype != 'SNV' else 1
	if vartype == 'SNV':
		return 'SNV'
	if length < 20:
		return vartype + '-small'
	elif 20 <= length < 50:
		return vartype + '-midsize'
	else:
		return vartype + '-large'


def run_count(vcf):
	counts = defaultdict(lambda: 0)
	for line in open(vcf, 'r'):
		if line.startswith('#'):
			print(line.strip())
			continue
		fields = line.split()
		info_fields = { k.split('=')[0] : k.split('=')[1] for k in fields[7].split(';') if '=' in k }
		assert 'ID' in info_fields
		ids = set([])
		for allele in info_fields['ID'].split(','):
			for id in allele.split(':'):
				ids.add(id)
		for id in ids:
			vartype = determine_type(id)
			if not vartype is None:
				counts[vartype] += 1
	for variant, count in counts.items():
		print('\t'.join([str(variant), str(count)])) 


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog='decomposition-helper.py', description=__doc__)
	subparsers = parser.add_subparsers(dest='subparser_name')
	parser_preprocess = subparsers.add_parser('prepare_input', help='prepare vcf to be used as input for decomposition.')

	parser_filter = subparsers.add_parser('filter', help='filter variants based on template VCF.')
	parser_filter.add_argument('-traversal', metavar='TRAVERSAL_VCF', help='traversal VCF.')
	parser_filter.add_argument('-snarls', metavar='SNARLS_VCF', help='snarls VCF.')

	parser_count = subparsers.add_parser('count', help='count alleles')
	parser_count.add_argument('-vcf', metavar='VCF', help='count alleles')

	args = parser.parse_args()

	if args.subparser_name == 'prepare_input':
		run_preprocessing()
	elif args.subparser_name == 'filter':
		run_filter(args.traversal, args.snarls)
	elif args.subparser_name == 'count':
		run_count(args.vcf)
