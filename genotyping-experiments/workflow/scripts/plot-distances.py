import matplotlib
matplotlib.use('pdf')
import numpy as np
import matplotlib.pyplot as plt
import argparse
import sys

parser = argparse.ArgumentParser(prog='plot-distances.py')
parser.add_argument('distances', metavar="DISTANCES", nargs='+', help="files produced by bedtools closest output (-d).")
parser.add_argument('outfile', metavar="OUTFILE", help="name of the output file.")
args = parser.parse_args()

chromosomes = ['chr' + str(i) for i in range(1,23)] + ['chrX'] 

callset_to_files = {}
sets1 = set([])
sets2 = set([])

for dist in args.distances:
	fields = dist.split('/')[-1].split('_')
	assert len(fields) > 3
	callset1 = fields[1]
	callset2 = fields[2]
	callset_to_files[(callset1, callset2)] = dist
	sets1.add(callset1)
	sets2.add(callset2)

assert sets1 == sets2

sets1 = sorted(list(sets1))
sets2 = sorted(list(sets2))

n = len(sets1)
fig = plt.figure(figsize=(15,12))
index = 1
for callset1 in sets1:
	for callset2 in sets2:
#		if callset1 == callset2:
#			index += 1
#			continue
		assert (callset1, callset2) in callset_to_files
		plt.subplot(n,n,index)
		filename = callset_to_files[(callset1, callset2)]
		histogram = []

		for line in open(filename, 'r'):
			if line.startswith('#'):
				continue
			fields = line.split()
			if not fields[0] in chromosomes:
				continue
			histogram.append(int(fields[-1]))

		plt.title(callset1 + '-' + callset2)
		plt.xlabel('distance to closest variant')
		plt.ylabel('count')

		plt.yscale('symlog')
		plt.xscale('symlog')

		bins = np.logspace(-1, 8, 200)
		bins = np.insert(bins, 0, 0.0, axis=0)

		plt.hist(histogram, bins=bins)
		index += 1

		# print the number of variants left and right of 1000
		n_left = len([h for h in histogram if h <= 1000])
		n_right = len([h for h in histogram if h > 1000])
		print('# variants with distance <= 1000: ' + str(n_left))
		print('# variants with distance > 1000: ' + str(n_right))


plt.subplots_adjust(top=0.96, bottom=0.08, left=0.10, right=0.90, hspace=0.3, wspace=0.3)
plt.savefig(args.outfile)
