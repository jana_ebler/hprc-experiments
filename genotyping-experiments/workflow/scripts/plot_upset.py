import sys, argparse
from collections import defaultdict
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import upsetplot
import pandas as pd

if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog='plot_upset.py', description=__doc__)
	parser.add_argument('-t', '--table', required=True, help='Table to which annotations shall be added.')
	parser.add_argument('-o', '--output', required=True, help='name of output pdf.')
	parser.add_argument('-n', '--names', nargs='+', default=[], required=True, help='Names of columns to include in plot.')
	parser.add_argument('-r', '--regions', nargs='+', default=[], help='regions to be considered.')
	args = parser.parse_args()

	# make sure to consider only chromosomes 1-22, chrX
	chromosomes = ['chr' + str(i) for i in range(1,23)] + ['chrX']
	
	df = pd.read_csv(args.table, sep='\t')
	df = df[df[args.names].any(1)]
	df = df[df.chromosome.isin(chromosomes)]

	with PdfPages(args.output) as pdf:
		plt.figure()
		variants = df.groupby(by=args.names).size()
		plt.figure()
		upsetplot.plot(variants, sort_by='cardinality', show_counts='%d')
		plt.suptitle('all regions')
		pdf.savefig()
		plt.close()


		for region in args.regions:
			plt.figure()
			df_inside = df[df[region + "_overlaps"]>=0.5]
			variants = df_inside.groupby(by=args.names).size()
			upsetplot.plot(variants, sort_by='cardinality', show_counts='%d')
			plt.suptitle('inside ' + region)
			pdf.savefig()
			plt.close()
