import sys
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from collections import defaultdict
from variantclassifier import VariantType, determine_variant_type, determine_type_from_alleles, determine_type_from_id

numbers_bubbles = defaultdict(int)
numbers_alleles = defaultdict(int)
numbers_ids = defaultdict(int)
numbers_allele_types = defaultdict(int)
numbers_ids_types = defaultdict(int)

varlengths = []
total_alleles = []

outname = sys.argv[1]

for line in sys.stdin:
	if line.startswith('#'):
		continue
	fields = line.split()
	alleles = [fields[3]] + [a for a in fields[4].split(',')]
	info_fields = { i.split('=')[0] : i.split('=')[1].strip() for i in fields[7].split(';') if '=' in i}
	nr_alleles = len(alleles) - 1
	varlen = max([len(a) for a in alleles])
	varids = set([])
	# type of the bubble
	vartype = determine_variant_type(line)
	for allele in alleles[1:]:
		# type of allele
		allele_type = determine_type_from_alleles(alleles[0], allele)
		numbers_allele_types[allele_type] += 1
	for id_string in info_fields['ID'].split(','):
		for id in id_string.split(':'):
			varids.add(id)
	varids = list(varids)
	for id in varids:
		# type of id
		id_type = determine_type_from_id(id)
		numbers_ids_types[id_type] += 1
	numbers_bubbles[vartype] += 1
	numbers_alleles[vartype] += nr_alleles
	numbers_ids[vartype] += len(varids)
	varlengths.append(varlen)
	total_alleles.append(len(varids))


variants = sorted([v for v in VariantType], key=lambda s: s.name)
print('\t'.join(['vartype', 'n_bubbles', 'n_alleles_before_decomposition (type determined by allele)', 'n_alleles_after_decomposition (type determined by allele)',  'n_alleles_before_decomposition (type determined by bubble)', 'n_alleles_after_decomposition (type determined by bubble)']))
for var in variants:
	print('\t'.join([var.name, str(numbers_bubbles[var]), str(numbers_allele_types[var]), str(numbers_ids_types[var]), str(numbers_alleles[var]), str(numbers_ids[var])]))


ax = sns.jointplot(x=varlengths, y=total_alleles, kind='hex', bins='log', xscale = 'log', yscale = 'log')
ax.set_axis_labels("bubble length (= length of longest allele)", "number of alleles in bubble after decomposition")
plt.colorbar()
plt.tight_layout()
plt.savefig(outname)	
