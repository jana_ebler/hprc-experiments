import unittest, importlib
from tempfile import TemporaryDirectory
import io
import os
from contextlib import redirect_stdout
from annotate_vcf import detect_variants, Node, ReferencePath, Node, define_id, parse_allele_traversal, traversal_to_string, reverse_complement, construct_allele_string, parse_gfa, preprocess_vcf, get_ref_position, decompose, validate, trim_alleles



class Test_Node(unittest.TestCase):
	def test_eq(self):
		node1 = Node(123, '>')
		node2 = Node(123, '>')
		node3 = Node(124, '>')
		
		self.assertEqual(node1, node2)
		self.assertEqual(node2, node1)
		self.assertFalse(node1 == node3)

		node4 = Node(123, '<')
		self.assertFalse(node1 == node4)

	def test_mark(self):
		node1 = Node(123, '>')
		self.assertFalse(node1.is_marked())
		node1.mark_node()
		self.assertTrue(node1.is_marked())


class Test_ReferencePath(unittest.TestCase):
	def test_contains_edge(self):
		traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		ref_path = ReferencePath(traversal)
		self.assertTrue(ref_path.contains_edge(Node(10161,'>'), Node(10162,'>')))
		self.assertTrue(ref_path.contains_edge(Node(10162,'>'),Node(10164,'>')))
		self.assertTrue(ref_path.contains_edge(Node(10164,'>'),Node(10168,'>')))

		self.assertFalse(ref_path.contains_edge(Node(10168,'>'), Node(10164,'>')))
		self.assertFalse(ref_path.contains_edge(Node(10162,'>'), Node(10162,'>')))
		self.assertFalse(ref_path.contains_edge(Node(10162,'<'), Node(10164,'>')))

	def test_contains_node(self):
		traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		ref_path = ReferencePath(traversal)
		for node in traversal:
			self.assertTrue(ref_path.contains_node(node))

	def test_get_subpath(self):
		traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		ref_path = ReferencePath(traversal)
		sub_path = ref_path.get_subpath(Node(10162,'>'), Node(10168,'>'))
		expected_path = [Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		self.assertEqual(sub_path, expected_path)	


class Test_define_id(unittest.TestCase):
	def test_id_ins(self):
		ref_allele = 'A'
		alt_allele = 'ATTAAAAAAAAA'
		ins_id = define_id(ref_allele, alt_allele, 'chr1', 1000, 1)
		self.assertEqual(ins_id, 'chr1-1000-INS-1-11')

	def test_id_del(self):
		ref_allele = 'ATTAAAAAAAAA'
		alt_allele = 'A'
		ins_id = define_id(ref_allele, alt_allele, 'chr1', 1000, 1)
		self.assertEqual(ins_id, 'chr1-1000-DEL-1-11')

	def test_id_complex(self):
		ref_allele = 'ATTAAAAAAAAA'
		alt_allele = 'ATT'
		ins_id = define_id(ref_allele, alt_allele, 'chr1', 1000, 1)
		self.assertEqual(ins_id, 'chr1-1000-COMPLEX-1-12')


class Test_detect_variants(unittest.TestCase):

	# alt allele contains several SNPs
	def test_detect1(self):
		ref_traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10165,'>'),Node(10167,'>'),Node(10168,'>'),Node(10170,'>'),Node(10171,'>'),Node(10173,'>'),Node(10174,'>'),Node(10175,'>'),Node(10176,'>'),Node(10177,'>')]
		ref_path = ReferencePath(ref_traversal)
		alt_traversal = [Node(10161,'>'),Node(10162,'>'),Node(10163,'>'),Node(10165,'>'),Node(10166,'>'),Node(10168,'>'),Node(10169,'>'),Node(10171,'>'),Node(10172,'>'),Node(10174,'>'),Node(10176,'>'),Node(10177,'>')]
		result = detect_variants(ref_traversal, alt_traversal)
		
		expected_alleles = [
			[Node(10162,'>'),Node(10163,'>'),Node(10165,'>')],
			[Node(10165,'>'),Node(10166,'>'),Node(10168,'>')],
			[Node(10168,'>'),Node(10169,'>'),Node(10171,'>')],
			[Node(10171,'>'),Node(10172,'>'),Node(10174,'>')],
			[Node(10174,'>'),Node(10176,'>')]
		]
		
#		print('computed')
#		for c in result:
#			print(traversal_to_string(c))
#		print('expected')
#		for e in expected_alleles:
#			print(traversal_to_string(e))

		self.assertEqual(result, expected_alleles)

	# alt allele contains duplication
	def test_detect2(self):
		ref_traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		ref_path = ReferencePath(ref_traversal)
		alt_traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		result = detect_variants(ref_traversal, alt_traversal)
		
		expected_alleles = [
			[Node(10164,'>'),Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		]

		self.assertEqual(result, expected_alleles)

	# alt allele contains insertion
	def test_detect3(self):
		ref_traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		ref_path = ReferencePath(ref_traversal)
		alt_traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10167,'>'), Node(10168,'>')]
		result = detect_variants(ref_traversal, alt_traversal)
		
		expected_alleles = [
			[Node(10164,'>'),Node(10167,'>'), Node(10168,'>')]
		]
		
		self.assertEqual(result, expected_alleles)


	# alt allele contains deletion
	def test_detect4(self):
		ref_traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		ref_path = ReferencePath(ref_traversal)
		alt_traversal = [Node(10161,'>'), Node(10168,'>')]
		result = detect_variants(ref_traversal, alt_traversal)
		
		expected_alleles = [
			[Node(10161,'>'),Node(10168,'>')]
		]

		self.assertEqual(result, expected_alleles)

	# alt allele contains inversion
	def test_detect5(self):
		ref_traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10168,'>')]
		ref_path = ReferencePath(ref_traversal)
		alt_traversal = [Node(10161,'>'), Node(10164,'<'), Node(10162,'<'),Node(10168,'>')]
		result = detect_variants(ref_traversal, alt_traversal)
		
		expected_alleles = [
			[Node(10161,'>'), Node(10164,'<'), Node(10162,'<'),Node(10168,'>')]
		]

		self.assertEqual(result, expected_alleles)

	# complex case
	def test_detect6(self):
		ref_traversal = [Node(1,'>'), Node(2,'>'),Node(3,'>'),Node(4,'>'), Node(5,'>')]
		ref_path = ReferencePath(ref_traversal)
		alt_traversal = [Node(1,'>'), Node(4,'>'),Node(2,'>'),Node(3,'>'), Node('x','>'), Node(4,'>'), Node(5, '>')]
		result = detect_variants(ref_traversal, alt_traversal)
		
		expected_alleles = [
			[Node(1,'>'), Node(4,'>'), Node(2,'>')],
			[Node(3,'>'), Node('x', '>'), Node(4,'>')]
		]

		self.assertEqual(result, expected_alleles)

	def test_detect7(self):
		ref_traversal = [Node(1,'>'), Node(2,'>'),Node(3,'>'),Node(4,'>'), Node(5,'>')]
		ref_path = ReferencePath(ref_traversal)
		alt_traversal = [Node(1,'>'), Node(2,'>'),Node(3,'>'),Node(6,'>'), Node('2','>'), Node(3,'>'), Node(5, '>')]
		result = detect_variants(ref_traversal, alt_traversal)
		
		expected_alleles = [
			[Node(3,'>'), Node(6,'>'), Node(2,'>'), Node(3,'>'), Node(5,'>')],
		]

		self.assertEqual(result, expected_alleles)

	def test_detect8(self):
		ref_traversal = parse_allele_traversal(">2533614>2533616>2533617>2533619>2533620")
		alt_traversal = parse_allele_traversal(">2533614<2533615>2533616>2533617<2533618>2533620")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">2533614<2533615>2533616"),
			parse_allele_traversal(">2533617<2533618>2533620")
		]

		self.assertEqual(result, expected_alleles)

	def test_detect9(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4")
		alt_traversal = parse_allele_traversal(">1>5>2>2>6>4")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">1>5>2"),
			parse_allele_traversal(">2>2>6>4")
		]
		self.assertEqual(result, expected_alleles)

	def test_detect10(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4")
		alt_traversal = parse_allele_traversal(">1>2>3>2>3>4>2>3>4")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">3>2>3>4>2>3>4")
		]

		self.assertEqual(result, expected_alleles)

	def test_detect11(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4")
		alt_traversal = parse_allele_traversal(">1>2>4>3>4>2>3>4")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">2>4>3"),
			parse_allele_traversal(">3>4>2>3>4")
		]

		self.assertEqual(result, expected_alleles)

	def test_detect12(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4")
		alt_traversal = parse_allele_traversal(">1>2>3>4>4")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">3>4>4")
		]

		self.assertEqual(result, expected_alleles)

	def test_detect13(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4")
		alt_traversal = parse_allele_traversal(">1>1>2>3>4")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">1>1>2")
		]

		self.assertEqual(result, expected_alleles)

	def test_detect14(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4")
		alt_traversal = parse_allele_traversal(">1>4>2>3>4")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">1>4>2")
		]

		self.assertEqual(result, expected_alleles)


	def test_detect15(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4")
		alt_traversal = parse_allele_traversal(">1>3>2>4")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">1>3>2"),
			parse_allele_traversal(">2>4")
		]

		self.assertEqual(result, expected_alleles)

	def test_detect16(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4")
		alt_traversal = parse_allele_traversal(">1>3>x>3>2>2>4")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">1>3>x>3>2"),
			parse_allele_traversal(">2>2>4")
		]
		self.assertEqual(result, expected_alleles)

	def test_detect17(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4>5")
		alt_traversal = parse_allele_traversal(">1>4>2>3>5")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">1>4>2"),
			parse_allele_traversal(">3>5")
		]
		self.assertEqual(result, expected_alleles)

	def test_detect18(self):
		ref_traversal = parse_allele_traversal(">1>2>3>4>5")
		alt_traversal = parse_allele_traversal(">1>4>3>2>5")
		result = detect_variants(ref_traversal, alt_traversal)

		expected_alleles = [
			parse_allele_traversal(">1>4>3>2"),
			parse_allele_traversal(">2>5")
		]
		self.assertEqual(result, expected_alleles)


class Test_parse_allele_traversal(unittest.TestCase):
	def test_parse_traversal(self):
		traversal_string = ">123>124>125>126<127<128>129"
		expected_traversal = [Node(123, '>'), Node(124, '>'), Node(125, '>'), Node(126, '>'), Node(127, '<'), Node(128, '<'), Node(129, '>')]
		computed_traversal = parse_allele_traversal(traversal_string)
		self.assertEqual(expected_traversal, computed_traversal)


class Test_traversal_to_string(unittest.TestCase):
	def test_to_string(self):
		traversal = [Node(123, '>'), Node(124, '>'), Node(125, '>'), Node(126, '>'), Node(127, '<'), Node(128, '<'), Node(129, '>')]
		expected_string = ">123>124>125>126<127<128>129"
		computed_string = traversal_to_string(traversal)
		self.assertEqual(expected_string, computed_string)


class Test_reverse_complement(unittest.TestCase):
	def test_complement1(self):
		seq = "ATGTCGCAATGACAGTTGCAGACGGTGTACCCCCCCCTGCGCGCGCTAGCAAGCAGCGGGCAGACGCCAGCAGCCCAGGC"
		expected = "GCCTGGGCTGCTGGCGTCTGCCCGCTGCTTGCTAGCGCGCGCAGGGGGGGGTACACCGTCTGCAACTGTCATTGCGACAT"
		result = reverse_complement(seq)
		self.assertEqual(expected, result)

	def test_complement2(self):
		seq = "ATTACCCAGCANNNNNNTCAGGCA"
		expected = "TGCCTGANNNNNNTGCTGGGTAAT"
		result = reverse_complement(seq)
		self.assertEqual(expected, result)

	def test_complement3(self):
		seq = "GGACCACACACACACACAGACGtcgctagctagCCGCAGCCTAGCC"
		expected = "GGCTAGGCTGCGGctagctagcgaCGTCTGTGTGTGTGTGTGGTCC"
		result = reverse_complement(seq)
		self.assertEqual(expected, result)


class Test_construct_allele(unittest.TestCase):
	def test_allele_construction1(self):
		traversal = [Node(123, '>'), Node(124, '>'), Node(125, '>'), Node(126, '>'), Node(127, '>'), Node(128, '>'), Node(129, '>')]
		gfa = {"123" : (0,"ATTG"), "124": (4, "GGATTAGTTAGT"), "125": (15, "GGAG"), "126": (18, "GG"), "127": (20, "TTGTG"), "128": (25, "AAAAA"), "129": (30, "AATGAT")}
		result = construct_allele_string(traversal, gfa)
		expected = "GGGATTAGTTAGTGGAGGGTTGTGAAAAA"
		self.assertEqual(result, expected)

	def test_allele_construction2(self):
		traversal = [Node(123, '<'), Node(124, '>'), Node(125, '>'), Node(126, '<'), Node(127, '>'), Node(128, '>'), Node(129, '>')]
		gfa = {"123" : (0,"ATTG"), "124": (4, "GGATTAGTTAGT"), "125": (15, "GGAG"), "126": (18, "GG"), "127": (20, "TTGTG"), "128": (25, "AAAAA"), "129": (30, "AATGAT")}
		result = construct_allele_string(traversal, gfa)
		expected = "TGGATTAGTTAGTGGAGCCTTGTGAAAAA"
		self.assertEqual(result, expected)

	def test_allele_construction3(self):
		traversal = [Node(123, '<'), Node(124, '<'), Node(125, '<'), Node(1, 'GGCGCGC')]
		gfa = {"123" : (0,"ATTG"), "124": (4, "GGATTAGTTAGT"), "125": (15, "GGAGGGAGGAGGCGCGAGCGCGTTGTGTCGCG")}
		result = construct_allele_string(traversal, gfa)
		expected = "TACTAACTAATCCCGCGACACAACGCGCTCGCGCCTCCTCCCTCC"
		self.assertEqual(result, expected)
		ref_pos = get_ref_position(traversal[0], gfa)
		self.assertEqual(ref_pos, '4')

	def test_allele_construction4(self):
		traversal = [Node(123, '<'), Node(124, '<'), Node(125, '<'), Node(1, 'GGCGCGC')]
		gfa = {"123" : (0,"ATTG"), "124": (4, "GGATTAGTTAGT"), "125": (15, "GGAGGGAGGAGGCGCGAGCGCGTTGTGTCGCG")}
		result = construct_allele_string(traversal, gfa, add_flank=False)
		expected = "ACTAACTAATCCCGCGACACAACGCGCTCGCGCCTCCTCCCTCC"
		self.assertEqual(result, expected)
		ref_pos = get_ref_position(traversal[0], gfa, add_flank=False)
		self.assertEqual(ref_pos, '5')


class Test_parse_gfa(unittest.TestCase):
	def test_parse_gfa(self):
		filename = "tempfile-gfa.txt"
		with open(filename, 'w') as gfa_file:
			gfa_file.write("S\tnode1\tATTAGGATAGAT\tSN:Z:MT_human\tSO:i:0\tSR:i:0\n")
			gfa_file.write("S\tnode2\tATTAGGATAGAT\tSN:Z:MT_human\tSO:i:10\tSR:i:0\n")
			gfa_file.write("S\tnode3\tATT\tSN:Z:MT_human\n")
		gfa = parse_gfa(filename)
		self.assertEqual(len(gfa), 3)
		self.assertTrue('node1' in gfa)
		self.assertTrue('node2' in gfa)
		self.assertTrue('node3' in gfa)
		self.assertEqual(gfa['node1'], ('0', 'ATTAGGATAGAT'))
		self.assertEqual(gfa['node2'], ('10', 'ATTAGGATAGAT'))
		self.assertEqual(gfa['node3'], (None, 'ATT'))
		os.remove(filename)


class Test_preprocess_vcf(unittest.TestCase):
	def test_preprocess(self):
		filename = "tempfile-vcf.vcf"
		with open(filename, 'w') as vcffile:
			vcffile.write("chr1\t23071\t>9827>9830\tT\tC\t60\t.\tAC=1;AF=1;AN=1;AT=>9827>9829>9830,>9827>9828>9830;LV=0;NS=1;PS=>32956>32957\n")
			vcffile.write("chr1\t90258\t>10187>10189\tA\tAGTCCCTCTGTCTCTGCCAACCAGTTAACCTGCTGCTTCCTGGAGGAAGACAGTCCCTCTGTCCCTCTGTCTCTGCCAACCAGTTAACCTGCTGCTTCCTGGAGGAAGACAGTCCCTCT\t60\t. \tAC=1;AF=1;AN=1;AT=>10187>10189,>10187>10188>10189;LV=0;NS=1;PS=>32956>32957\n")
		segments = preprocess_vcf(filename)
		expected_segments = ["9827", "9828", "9829", "9830", "10187", "10189", "10187", "10188"]
		for e in expected_segments:
			self.assertTrue(e in segments)
		os.remove(filename)


class Test_validate(unittest.TestCase):
	def test_validate(self):
		ref_traversal = [Node(10161,'>'), Node(10162,'>'),Node(10164,'>'),Node(10165,'>'),Node(10167,'>'),Node(10168,'>'),Node(10170,'>'),Node(10171,'>'),Node(10173,'>'),Node(10174,'>'),Node(10175,'>'),Node(10176,'>'),Node(10177,'>')]
		alt_traversal = [Node(10161,'>'),Node(10162,'>'),Node(10163,'>'),Node(10165,'>'),Node(10166,'>'),Node(10168,'>'),Node(10169,'>'),Node(10171,'>'),Node(10172,'>'),Node(10174,'>'),Node(10176,'>'),Node(10177,'>')]
		alleles = [
			[Node(10162,'>'),Node(10163,'>'),Node(10165,'>')],
			[Node(10165,'>'),Node(10166,'>'),Node(10168,'>')],
			[Node(10168,'>'),Node(10169,'>'),Node(10171,'>')],
			[Node(10171,'>'),Node(10172,'>'),Node(10174,'>')],
			[Node(10174,'>'),Node(10176,'>')]
		]
		validate(ref_traversal, alleles, alt_traversal)

class Test_decompose(unittest.TestCase):
	def test_decompose1(self):
		# in this testcase, there are different sub-traversals representing the same DNA sequence, thus, allele should be identified only once
		gfa = {'A': ('0', 'A'), 'B': ('1', 'A'), 'C': ('2','T'), 'D': (None, 'A'), 'E': ('3', 'G'), 'F': ('4', 'T'), 'G': ('5', 'C'), 'I': (None, 'G'), 'H': (None, 'G')}
		line = "chr22\t0\t>A>G\tAATGTC\tAATGGC,AATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G,>A>B>C>E>I>G;LV=0\tGT\t0|1\t2|2"
		comp_multi, comp_bi = decompose(line, gfa)
		expected_multi = "chr22\t0\t>A>G\tAATGTC\tAATGGC,AATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G,>A>B>C>E>I>G;LV=0;ID=chr22-5-SNV->E>H>G-1,chr22-5-SNV->E>H>G-1\tGT\t0|1\t2|2"
		expected_bi = ["chr22\t5\t>A>G\tT\tG\t.\tPASS\tAT=>E>F>G,>E>H>G;ID=chr22-5-SNV->E>H>G-1\tGT\t0|1\t1|1"]
		self.assertEqual(comp_multi, expected_multi)
		self.assertEqual(comp_bi, expected_bi)

	def test_decompose2(self):
		gfa = {'A': ('0', 'A'), 'B': ('1', 'A'), 'C': ('2','T'), 'D': (None, 'A'), 'E': ('3', 'G'), 'F': ('4', 'T'), 'G': ('5', 'C'), 'I': (None, 'A'), 'H': (None, 'G')}
		line = "chr22\t0\t>A>G\tAATGTC\tAATGGC,AATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G,>A>B>C>E>I>G;LV=0\tGT\t0|1\t2|2"
		comp_multi, comp_bi = decompose(line, gfa)
		expected_multi = "chr22\t0\t>A>G\tAATGTC\tAATGGC,AATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G,>A>B>C>E>I>G;LV=0;ID=chr22-5-SNV->E>H>G-1,chr22-5-SNV->E>I>G-1\tGT\t0|1\t2|2"
		expected_bi = ["chr22\t5\t>A>G\tT\tG\t.\tPASS\tAT=>E>F>G,>E>H>G;ID=chr22-5-SNV->E>H>G-1\tGT\t0|1\t0|0", "chr22\t5\t>A>G\tT\tA\t.\tPASS\tAT=>E>F>G,>E>I>G;ID=chr22-5-SNV->E>I>G-1\tGT\t0|0\t1|1"]
		self.assertEqual(comp_multi, expected_multi)
		self.assertEqual(comp_bi, expected_bi)

	def test_decompose3(self):
		# biallelic record, only ID will be added
		gfa = {'A': ('0', 'A'), 'B': ('1', 'A'), 'C': ('2','T'), 'D': (None, 'A'), 'E': ('3', 'G'), 'F': ('4', 'T'), 'G': ('5', 'C'), 'I': (None, 'A'), 'H': (None, 'G')}
		line = "chr22\t0\t>A>G\tAATGTC\tAATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G;LV=0\tGT\t0|1\t1|1"
		comp_multi, comp_bi = decompose(line, gfa)
		expected_multi = "chr22\t0\t>A>G\tAATGTC\tAATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G;LV=0;ID=chr22-0-COMPLEX->A>D>C>E>H>G-6\tGT\t0|1\t1|1"
		expected_bi = ["chr22\t0\t>A>G\tAATGTC\tAATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G;LV=0;ID=chr22-0-COMPLEX->A>D>C>E>H>G-6\tGT\t0|1\t1|1"]
		self.assertEqual(comp_multi, expected_multi)
		self.assertEqual(comp_bi, expected_bi)

	def test_decompose4(self):
		# in this testcase, there are different sub-traversals representing the same DNA sequence, thus, allele should be identified only once
		gfa = {'A': ('0', 'A'), 'B': ('1', 'A'), 'C': ('2','T'), 'D': (None, 'G'), 'E': ('3', 'G'), 'F': ('4', 'T'), 'G': ('5', 'C'), 'I': (None, 'G'), 'H': (None, 'G')}
		line = "chr22\t0\t>A>G\tAATGTC\tAATGGC,AATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G,>A>B>C>E>I>G;LV=0\tGT\t0|1\t2|2"
		comp_multi, comp_bi = decompose(line, gfa)
		expected_multi = "chr22\t0\t>A>G\tAATGTC\tAATGGC,AATGGC\t60\t.\tAT=>A>B>C>E>F>G,>A>D>C>E>H>G,>A>B>C>E>I>G;LV=0;ID=chr22-2-SNV->A>D>C-1:chr22-5-SNV->E>H>G-1,chr22-5-SNV->E>H>G-1\tGT\t0|1\t2|2"
		expected_bi = ["chr22\t2\t>A>G\tA\tG\t.\tPASS\tAT=>A>B>C,>A>D>C;ID=chr22-2-SNV->A>D>C-1\tGT\t0|1\t0|0", "chr22\t5\t>A>G\tT\tG\t.\tPASS\tAT=>E>F>G,>E>H>G;ID=chr22-5-SNV->E>H>G-1\tGT\t0|1\t1|1"]
		self.assertEqual(comp_multi, expected_multi)
		self.assertEqual(comp_bi, expected_bi)

class Test_trim_alleles(unittest.TestCase):
	def test_trim(self):
		ref = "ATGC"
		alt = "ATG"
		pos = '0'
		c_pos, c_ref, c_alt = trim_alleles(pos, ref, alt)
		self.assertEqual(c_pos, '2')
		self.assertEqual(c_ref, 'GC')
		self.assertEqual(c_alt, 'G')
	def test_trim2(self):
		ref = 'AT'
		alt = 'AG'
		pos = '0'
		c_pos, c_ref, c_alt = trim_alleles(pos, ref, alt)
		self.assertEqual(c_pos, '1')
		self.assertEqual(c_ref, 'T')
		self.assertEqual(c_alt, 'G')
	def test_trim3(self):
		ref = "GCTGTT"
		alt = "GCTAAATT"
		pos = '0'
		c_pos, c_ref, c_alt = trim_alleles(pos, ref, alt)
		self.assertEqual(c_pos, '3')
		self.assertEqual(c_ref, 'G')
		self.assertEqual(c_alt, 'AAA')

