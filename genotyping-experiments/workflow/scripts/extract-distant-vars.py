import sys

threshold = int(sys.argv[1])

print("##fileformat=VCFv4.2")
print("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO")

for line in sys.stdin:
	if line.startswith('#'):
		continue
	fields = line.strip().split()
	distance = int(fields[-1])
	if distance <= threshold:
		continue
	# print only first vcf record (excluding genotypes)
	print('\t'.join(fields[:8]))	
