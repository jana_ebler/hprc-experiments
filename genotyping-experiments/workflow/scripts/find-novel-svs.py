import sys
import argparse
import gzip
from collections import defaultdict

def parse_ids(filename):
	ids = defaultdict(lambda: False)
	for line in open(filename, 'r'):
		varid = line.strip()
		ids[varid] = True
	return ids


def parse_panel(novel, lenient, samples, outname):
	sys.stderr.write('\t'.join(['var_id', 'in_samples', 'in_lenient', 'in_novel']) + '\n')
	sample_ix = []

	with open(outname + '-novellenient.tsv', 'w') as a, open(outname + '-knownlenient.tsv', 'w') as b, open(outname + '-lenient.tsv', 'w') as c:
		for line in sys.stdin:
			if line.startswith('##'):
				continue
			fields = line.strip().split()
			if line.startswith('#'):
				for sample in samples:
					assert sample in fields
					sample_ix.append(fields.index(sample))
				continue
			assert len(sample_ix) == len(samples)
			info_fields = { k.split('=')[0] : k.split('=')[1] for k in fields[7].split(';') if '=' in k }
			assert 'ID' in info_fields
			varid = info_fields['ID']
			# check if variant in any of the given samples
			is_present = False
			for index in sample_ix:
				assert fields[index] in ['1|.', '1|1', '1|0', '0|1', '.|1', '.|0', '0|.', '0|0', '.|.']
				if fields[index] in ['1|.', '1|1', '1|0', '0|1', '.|1']:
					is_present = True
					break
			sys.stderr.write('\t'.join([varid, str(is_present), str(lenient[varid]), str(novel[varid])]) + '\n')
			if not (is_present and lenient[varid] and novel[varid]):
				# complement of novel+lenient SVs
				a.write(varid + '\n')
			if not (is_present and lenient[varid] and not novel[varid]):
				# complement of known+lenient SVs
				b.write(varid + '\n')
			if not (is_present and lenient[varid]):
				# complement of lenient SVs
				c.write(varid + '\n')


def compute_table(lenient, novel, samples, outname):
	novel_ids = parse_ids(novel)
	lenient_ids = parse_ids(lenient)
	parse_panel(novel_ids, lenient_ids, samples, outname)



if __name__ == "__main__":
	parser = argparse.ArgumentParser(prog='find-novel-svs.py', description=__doc__)
	parser.add_argument('lenient', metavar='LENIENT', help='list of lenient ids.')
	parser.add_argument('novel', metavar='NOVEL', help='list of novel ids.')
	parser.add_argument('-samples', metavar='SAMPLES', nargs='+', help='samples to consider')
	parser.add_argument('-outname', metavar='OUTNAME', help='prefix of output files')
	args = parser.parse_args()
	
	compute_table(args.lenient, args.novel, [s for s in args.samples], args.outname)
