import sys

genome_index = sys.argv[1]

chrom_to_length = {}
for line in open(genome_index, 'r'):
	fields = line.split()
	chrom_to_length[fields[0]] = fields[1]

counter = 0

chromosomes_hg38 = ['chr' + str(i) for i in range(1,23)] + ['chrX']
chromosomes_hg37 = [str(i) for i in range(1,23)] + ['X']

for line in sys.stdin:
	if line.startswith('#'):
		if line.startswith('##contig=<'):
			# add chromosome length, this is required by truvari..
			f = {s.split('=')[0] : s.split('=')[1] for s in line.strip()[10:-1].split(',') }
			assert 'ID' in f
			chrom = f['ID']
			if not chrom in chromosomes_hg38 and not chrom in chromosomes_hg37:
				continue
			if not 'length' in f:
				assert chrom in chrom_to_length
				line = "##contig=<ID=" + chrom + ',length=' + chrom_to_length[chrom] + '>'
		print(line.strip())
		continue
	fields = line.split()
	if fields[0] not in chromosomes_hg38 and fields[0] not in chromosomes_hg37:
		continue
	ref_allele = fields[3]
	alt_alleles = fields[4].split(',')
	varlen = max([len(a) for a in [ref_allele] + alt_alleles])
	if varlen >= 50:
		counter += 1
		print(line.strip())

sys.stderr.write('Extracted ' + str(counter) + ' SVs.\n')
