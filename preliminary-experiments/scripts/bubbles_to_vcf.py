import sys
import argparse
from collections import defaultdict


class Bubble:
	def __init__(self, chrom, start, end, segments):
		"""
		start -- start coordinate
		end -- end coordinate
		segments -- list of segments in the bubble (first = source, last = sink)
		"""
		self.chrom = chrom
		self.start = start
		self.end = end
		assert len(segments) > 1
		self.segments = segments
	
	def get_source(self):
		return self.segments[0]

	def get_sink(self):
		return self.segments[-1]

	def __str__(self):
		s = '{' + ','.join(self.segments) + '}'
		return ','.join(['Bubble(' + self.chrom, self.start, self.end, s + ')'])

	def __eq__(self, other):
		if self.chrom != other.chrom:
			return False
		if self.start != other.start:
			return False
		if self.end != other.end:
			return False
		if self.segments != other.segments:
			return False
		return True


class Path:
	def __init__(self, path_id, sample, haplotype, segments, orientations, is_ref = False):
		"""
		path_id -- id
		sample -- sample this path corresponds to
		haplotype -- haplotype this path corresponds to
		segments -- the segments on the path (in correct order)
		orientations -- orientations of the segments in the path
		is_ref -- whether this path represents the reference genome
		"""
		self.path_id = path_id
		self.sample = sample
		self.haplotype = haplotype
		self.segments = segments
		self.orientations = orientations
		self.is_ref = is_ref

	def __str__(self):
		s = '{' + ','.join(self.segments) + '}'
		return ','.join(['Path(' + self.path_id, self.sample, self.haplotype, s + ')'])

#	def get_subpath(self, start_segment, end_segment):
#		"""
#		Return sub path starting at leftmost occurance of
#		start_segment and ending at rightmost occurance of
#		end_segment.
#		"""
#		start_pos = None
#		end_pos = None
#		
#		for i,s in enumerate(self.segments):
#			if (start_pos is None) and (s == start_segment):
#				start_pos = i
#				continue
#			if s == end_segment:
#				end_pos = i
#		
#		if (start_pos is None) or (end_pos is None):
#			return [], []
#		
#		return self.segments[start_pos:end_pos], self.orientations[start_pos:end_pos]

	def get_subpath(self, start_segment, end_segment):
		"""
		Return sub path starting at start_segment and
		ending and end_segment
		"""
		start_pos = None
		end_pos = None
		# look for left and rightmost source/sink
		for i,s in enumerate(self.segments):
			if s == start_segment or s == end_segment:
				if start_pos is None:
					start_pos = i
				end_pos = i
		if start_pos is None or end_pos is None:
			return [],[]

		# if subpath consists of a single element, return the element
		if start_segment == end_segment and start_pos == end_pos:
			return [start_segment], [False]

		if self.segments[start_pos] == start_segment and self.segments[end_pos] == end_segment:
			assert start_pos < end_pos
			return self.segments[start_pos:end_pos], self.orientations[start_pos:end_pos]
		elif self.segments[start_pos] == end_segment and self.segments[end_pos] == start_segment:
			assert start_pos < end_pos
			return self.segments[start_pos+1:end_pos+1][::-1], [not o for o in self.orientations[start_pos+1:end_pos+1][::-1]]
		else:
			return [],[]
			

	def __lt__(self, other):
		return self.is_ref and (not other.is_ref)


def extract_bubbles_from_bed_gfatools(filename):
	"""
	Reads a bed file produced by gfatools bubble specifying bubbles
	and constructs a list of Bubble objects.
	"""
	result = []
	segments = {}
	for line in open(filename, 'r'):
		fields = line.split()
		chrom = fields[0]
		start = fields[1]
		end = fields[2]
		bubble_segments = []
		for s in fields[11].split(','):
			bubble_segments.append(s)
			segments[s] = None
		result.append(Bubble(chrom, start, end, bubble_segments))
	return result, segments


def extract_bubbles_from_bed(filename):
	"""
	Reads a bed file specifying bubbles and constructs a list
	of Bubble objects.
	<chrom> <start> <end> <source> <sink> <inner nodes>
	"""
	result = []
	segments = {}
	for line in open(filename, 'r'):
		fields = line.split()
		chrom = fields[0]
		start = fields[1]
		end = fields[2]
		bubble_segments = [fields[3]]
		for s in fields[5].split(','):
			bubble_segments.append(s)
			segments[s] = None
		bubble_segments.append(fields[4])
		segments[fields[3]] = None
		segments[fields[4]] = None
		result.append(Bubble(chrom, start, end, bubble_segments))
	return result, segments


def reverse_complement(sequence):
	"""
	Computes the reverse complement of a sequence.
	"""
	result = ''
	complement = {'A':'T', 'T':'A', 'C':'G', 'G':'C', 'a':'t', 't':'a', 'c':'g', 'g':'c'}
	for b in sequence:
		result += complement[b]
	return result[::-1]


def construct_sequence(segments, orientations, segment_to_string):
	"""
	Construct sequence corresponding to a sequence of segments.
	Assumes that the segments are NOT overlapping.
	"""
	result = ''
	for segment, orientation in zip(segments, orientations):
		sequence = segment_to_string[segment] if not orientation else reverse_complement(segment_to_string[segment])
		result += sequence
	return result


def left_align(alleles, length):
	"""
	Left align the allele sequences (removing prefixes occuring in all).
	Adds a padding base if necessary.
	"""
	if (all([len(a) > length for a in alleles])):
		return [a[length:] for a in alleles], False
	else:
		return [a[length-1:] for a in alleles], True


def parse_walk(path_string):
	"""
	Extracts the path segments and their orientations
	from a W line in GFA.
	"""
	# determine positions of delimiters <,>
	positions = []
	for i,s in enumerate(path_string):
		if s in ['>', '<']:
			positions.append(i)
	positions.append(len(path_string))
	path_segments = []
	path_orientations = []
	for i in range(len(positions)-1):
		segment = path_string[positions[i]:positions[i+1]]
		assert segment[0] == '>' or segment[0] == '<'
		path_orientations.append(segment[0] == '<')
		path_segments.append(segment[1:])
	return path_segments, path_orientations


def parse_path(path_string):
	"""
	Extracts the path segments and their orientations
	from a P line in GFA.
	"""
	segments = path_string.split(',')
	path_segments = []
	path_orientations = []
	for s in segments:
		if '+' in s:
			path_orientations.append(False)
		elif '-' in s:
			path_orientations.append(True)
		else:
			assert False
		path_segments.append(s[:-1])
	return path_segments, path_orientations


def parse_gfa(filename, segments):
	"""
	Reads relevant information from GFA file.
	Extracts sequences of segments given and returns
	samples and paths present in the graph.
	"""
	paths = []
	samples = set([])
	# parse the gfa to get segments and paths
	for line in open(filename, 'r'):
		if not (line[0] in ['S', 'P', 'W']):
			continue
		fields = line.split()
		if fields[0] == 'S':
			# segment (only keep if in a bubble)
			if fields[1] in segments:
				segments[fields[1]] = fields[2]
			continue
		if fields[0] == 'W':
			# Walk line
			samples.add(fields[1])
			path_id = '-'.join([fields[1], fields[2], fields[3]])
			path_segments, path_orientations = parse_walk(fields[6])
			paths.append(Path(path_id, fields[1], fields[2], path_segments, path_orientations))
			continue
		if fields[0] == 'P':
			# Path line. Assume only reference is represented as path (P line)
			path_id = fields[1]
			path_segments, path_orientations = parse_path(fields[2])
			paths.append(Path(path_id, path_id, '0', path_segments, path_orientations, True))
	return paths, samples


def construct_vcf_records(bubbles, paths, samples):
	"""
	Constructs a phased VCF from the bubbles, samples and
	their respective paths.
	"""
	# print header of VCF
	print('##fileformat=VCFv4.2')
	print('#FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">')
	print('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t' + '\t'.join(samples))
	nr_variants = 0
	# construct variants
	for bubble in bubbles:
		# all allele sequences through this bubble
		alleles = [None]
		haplotypes = defaultdict(lambda: ['.', '.'])
		for path in paths:
			# get sequence between source and sink
			segment_path, orientations = path.get_subpath(bubble.get_source(), bubble.get_sink())
			if len(segment_path) == 0:
				# path does not cover this bubble
				continue
			# construct allele sequence in this bubble
			allele_sequence = construct_sequence(segment_path, orientations, segments)
			allele_id = 0
			if path.is_ref:
				assert alleles[0] is None
				assert len(alleles) == 1
				alleles[0] = allele_sequence
			else:
				if (not allele_sequence in alleles):
					alleles.append(allele_sequence)
				allele_id = alleles.index(allele_sequence)
			haplotypes[path.sample][int(path.haplotype)] = str(allele_id)
		assert alleles[0] is not None
		if len(alleles) < 2:
			sys.stderr.write('Skipping bubble at ' + bubble.chrom + ':' + bubble.start  + '-' + bubble.end + ' since no alternative allele is covered.\n')
			continue
		haplotype_string = []
		for sample in samples:
			haplotype_string.append(haplotypes[sample][0] + '|' + haplotypes[sample][1])
		# left align variants and add padding base if necessary
		allele_sequences, padding_base_added = left_align(alleles, len(segments[bubble.get_source()]))
		var_start = int(bubble.start) - 1 if padding_base_added else int(bubble.start)
		print('\t'.join([bubble.chrom, str(var_start), '.', allele_sequences[0], ','.join(allele_sequences[1:]), '.', 'PASS', '.', 'GT', '\t'.join(haplotype_string)]))
		nr_variants += 1
	return nr_variants


if __name__ == "__main__":
	parser = argparse.ArgumentParser(prog='bubbles_to_vcf.py', description=__doc__)
	parser.add_argument('-bed', metavar='BED', required=True, help='BED file containing bubbles.')
	parser.add_argument('-gfa', metavar='GFA', required=True, help='GFA file with assembly walks and reference paths.')
	parser.add_argument('--gfatools', action='store_true', help='Use this flag if input BED file was produced by gfatools bubble.')
	args = parser.parse_args()

	# read bubbles and respective segments
	bubbles, segments = extract_bubbles_from_bed_gfatools(args.bed) if args.gfatools else extract_bubbles_from_bed(args.bed)
	sys.stderr.write('Read ' + str(len(bubbles)) + ' bubbles from the input BED file.\n')
	# read the graph from GFA
	paths, samples = parse_gfa(args.gfa, segments)
	samples = list(samples)
	# make sure reference paths come first in list
	paths.sort()
	sys.stderr.write('Read ' + str(len(paths)) + ' paths from the GFA file.\n')
	# create the output VCF file
	nr_variants = construct_vcf_records(bubbles, paths, samples)
	sys.stderr.write('Wrote ' + str(nr_variants) + ' variants to the output VCF file.\n')
