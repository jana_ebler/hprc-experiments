import unittest
import importlib
from tempfile import TemporaryDirectory
import io
from contextlib import redirect_stdout

from bubbles_to_vcf import Bubble, Path, extract_bubbles_from_bed_gfatools, extract_bubbles_from_bed, reverse_complement, construct_sequence, left_align, parse_walk, parse_path, parse_gfa

class Test_bubble_class(unittest.TestCase):
	def test_bubble(self):
		bubble = Bubble('chr1', '1', '2', ['s1', 's2', 's3'])
		self.assertEqual(bubble.get_source(), 's1')
		self.assertEqual(bubble.get_sink(), 's3')

class Test_path_class(unittest.TestCase):
	def test_subpath_forward(self):
		path = Path('path1', 'sample1', '0', ['s1', 's2', 's3', 's4', 's5'], [False, True, False, False, True], False)
		subpath, orientations = path.get_subpath('s2', 's4')
		# including first node, excluding last
		self.assertEqual(subpath, ['s2', 's3'])
		self.assertEqual(orientations, [True, False])
		subpath, orientations = path.get_subpath('s1', 's5')
		self.assertEqual(subpath, ['s1', 's2', 's3', 's4'])
		self.assertEqual(orientations, [False, True, False, False])

	def test_subpath_reverse(self):
		path = Path('path1', 'sample1', '0', ['s1', 's2', 's3', 's4', 's5'], [False, True, False, False, True], False)
		subpath, orientations = path.get_subpath('s4', 's2')
		# including first node, excluding last
		self.assertEqual(subpath, ['s4', 's3'])
		self.assertEqual(orientations, [True, True])
		subpath, orientations = path.get_subpath('s5', 's1')
		self.assertEqual(subpath, ['s5', 's4', 's3', 's2'])
		self.assertEqual(orientations, [False, True, True, False])

	def test_subpath_absent(self):
		path = Path('path1', 'sample1', '0', ['s1', 's2', 's3', 's4', 's5'], [False, True, False, False, True], False)
		subpath, orientations = path.get_subpath('s2', 's6')
		# including first node, excluding last
		self.assertEqual(subpath, [])
		self.assertEqual(orientations, [])

	def test_subpath_emtpy(self):
		path = Path('path1', 'sample1', '0', ['s1', 's2', 's3', 's4', 's5'], [False, True, False, False, True], False)
		subpath, orientations = path.get_subpath('s2', 's2')
		# including first node, excluding last
		self.assertEqual(subpath, ['s2'])
		self.assertEqual(orientations, [False])
	
	def test_lt(self):
		path1 = Path('path1', 'sample1', '0', ['s1', 's2', 's3', 's4', 's5'], [False, True, False, False, True], False)
		path2 = Path('path2', 'sample2', '0', ['s1', 's2', 's3', 's4', 's5'], [False, True, False, False, True], True)
		path3 = Path('path3', 'sample3', '0', ['s1'], [False], False)
		self.assertEqual(path1 < path2, False)
		self.assertEqual(path1 > path2, True)
		self.assertEqual(path2 < path1, True)
		self.assertEqual(path3 < path1, False)
		self.assertEqual(path1 < path3, False)

class Test_read_bed(unittest.TestCase):
	def test_extract_bubbles_from_bed_gfatools(self):
		bubbles, segments = extract_bubbles_from_bed_gfatools('testdata/gfatools.bed')
		self.assertEqual(len(bubbles), 3)
		expected_segments = ['MTh0', 'MTo3426', 'MTh4001', 'MTh4502', 'MTo8961', 'MTh9505', 'MTh13014', 'MTh13516']
		self.assertEqual(len(segments), 8)
		for s in expected_segments:
			self.assertTrue(s in segments)
		expected_bubbles = [
					Bubble('MT_human', '4001', '4502', ['MTh0', 'MTo3426', 'MTh4001', 'MTh4502']),
					Bubble('MT_human', '9505', '9505', ['MTh4502', 'MTo8961', 'MTh9505']),
					Bubble('MT_human', '13014', '13516', ['MTh9505', 'MTh13014', 'MTh13516'])
				]
		self.assertEqual(bubbles, expected_bubbles)

	def test_extract_bubbles_from_bed(self):
		bubbles, segments = extract_bubbles_from_bed('testdata/bubblegun.bed')
		self.assertEqual(len(bubbles), 2)
		expected_segments = ['MTh4502', 'MTo8961', 'MTh9505', 'MTh13014', 'MTh13516']
		self.assertEqual(len(segments), 5)
		for s in expected_segments:
			self.assertTrue(s in segments)
		expected_bubbles = [
					Bubble('MT_human', '9505', '9505', ['MTh4502', 'MTo8961', 'MTh9505']),
					Bubble('MT_human', '13014', '13516', ['MTh9505', 'MTh13014', 'MTh13516'])
				]
		self.assertEqual(bubbles, expected_bubbles)

class Test_reverse_complement(unittest.TestCase):
	def test_reverse_complement(self):
		sequence = 'ATGATAGACAGACGTTCGCTAGCAGCCGATAGCCAGATCGCTAGGCTCTCTCGATAGAGATTCTCGGGTATAGTCTGGTC'
		expected = 'GACCAGACTATACCCGAGAATCTCTATCGAGAGAGCCTAGCGATCTGGCTATCGGCTGCTAGCGAACGTCTGTCTATCAT'
		self.assertEqual(reverse_complement(sequence), expected)


class Test_construct_sequence(unittest.TestCase):
	def test_construct_sequence1(self):
		# construct_sequence(segments, orientations, segment_to_string):
		segments = ['s1', 's2', 's1', 's3']
		orientations = [False, False, True, False]
		segment_to_string = {'s1': 'AAAT', 's2': 'CGCGC', 's3': 'GGGCGCGCGAA'}
		expected = 'AAATCGCGCATTTGGGCGCGCGAA'
		self.assertEqual(construct_sequence(segments, orientations, segment_to_string), expected)

	def test_construct_sequence2(self):
		# construct_sequence(segments, orientations, segment_to_string):
		segments = ['s1', 's2', 's3']
		orientations = [True, True, True]
		segment_to_string = {'s1': 'AAAT', 's2': 'CGCGC', 's3': 'GGGCGCGCGAA'}
		expected = 'ATTTGCGCGTTCGCGCGCCC'
		self.assertEqual(construct_sequence(segments, orientations, segment_to_string), expected)

class Test_left_align(unittest.TestCase):
	def test_left_align(self):
		alleles = ['AAAAT', 'AAAAA', 'AAAAC']
		normalized, added = left_align(alleles, 4)
		self.assertEqual(normalized, ['T', 'A', 'C'])
		self.assertFalse(added)
		
		alleles = ['ATGGCAAT', 'ATGGC']
		normalized, added = left_align(alleles, 5)
		self.assertEqual(normalized, ['CAAT', 'C'])
		self.assertTrue(added)

class Test_parse_walk(unittest.TestCase):
	def test_parse_walk(self):
		walk_string = '>s11<s12>s13'
		path_segments, path_orientations = parse_walk(walk_string)
		self.assertEqual(path_segments, ['s11', 's12', 's13'])
		self.assertEqual(path_orientations, [False, True, False])

class Test_parse_path(unittest.TestCase):
	def test_parse_path(self):
		walk_string = '11+,12-,13+'
		path_segments, path_orientations = parse_path(walk_string)
		self.assertEqual(path_segments, ['11', '12', '13'])
		self.assertEqual(path_orientations, [False, True, False])

class Test_parse_gfa(unittest.TestCase):
	def test_parse_gfa(self):
		segments = {'MTh0': None, 'MTh4001': None, 'MTo3426': None}
		paths, samples = parse_gfa('testdata/testfile.gfa', segments)
		self.assertEqual(len(segments), 3)
		self.assertEqual(len(segments['MTh0']), 4001)
		self.assertEqual(len(paths), 6)
